#!/bin/sh

JVM_OPTS="-XX:+UseG1GC \
-XX:+UseStringDeduplication \
-XX:+ParallelRefProcEnabled \
-XX:+AlwaysPreTouch \
-XX:+DisableExplicitGC \
-XX:ParallelGCThreads=6 \
-XX:GCTimeRatio=9 \
-XX:MaxGCPauseMillis=25 \
-XX:ConcGCThreads=6 \
-XX:InitiatingHeapOccupancyPercent=35 \
-XX:MaxTenuringThreshold=10 \
-XX:SurvivorRatio=6 \
-XX:-UseAdaptiveSizePolicy \
-XX:MaxMetaspaceSize=256M \
-Xmx6G \
-Xms2G"

LOG_CONF_FILE="./logback-spring.xml"
CONFIG_PATH="/app/config/release.yml"

java $JVM_OPTS -jar *.jar -Dlogging.config=$LOG_CONF_FILE --spring.config.location=$CONFIG_PATH
