package io.karhit.ecommerce.mapper;

import io.karhit.ecommerce.entity.OrderProductDetailEntity;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class OrderProductDetailMapper implements RowMapper<OrderProductDetailEntity> {

  @Override
  public OrderProductDetailEntity mapRow(ResultSet resultSet, int i) throws SQLException {
    return mapOrderProductDetail(resultSet);
  }

  public static OrderProductDetailEntity mapOrderProductDetail(ResultSet resultSet) throws SQLException {
    return OrderProductDetailEntity.builder()
        .id(resultSet.getLong("id"))
        .orderStoreDetailId(resultSet.getLong("order_store_detail_id"))
        .productId(resultSet.getLong("product_id"))
        .productName(resultSet.getString("product_name"))
        .quantity(resultSet.getInt("quantity"))
        .price(resultSet.getBigDecimal("price"))
        .totalPrice(resultSet.getBigDecimal("total_price"))
        .build();
  }
}
