package io.karhit.ecommerce.mapper;

import io.karhit.ecommerce.entity.Enum.OrderStatus;
import io.karhit.ecommerce.entity.OrderStoreDetailEntity;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class OrderStoreDetailMapper implements RowMapper<OrderStoreDetailEntity> {

  @Override
  public OrderStoreDetailEntity mapRow(ResultSet resultSet, int i) throws SQLException {
    return mapOrderStoreDetail(resultSet);
  }

  public static OrderStoreDetailEntity mapOrderStoreDetail(ResultSet resultSet) throws SQLException {
    return OrderStoreDetailEntity.builder()
        .id(resultSet.getLong("id"))
        .orderId(resultSet.getLong("order_id"))
        .storeId(resultSet.getLong("store_id"))
        .storeName(resultSet.getString("store_name"))
        .totalPrice(resultSet.getBigDecimal("total_price"))
        .status(OrderStatus.valueOf(resultSet.getString("status").toUpperCase()))
        .build();
  }
}
