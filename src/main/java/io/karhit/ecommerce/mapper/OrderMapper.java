package io.karhit.ecommerce.mapper;

import io.karhit.ecommerce.entity.Enum.OrderStatus;
import io.karhit.ecommerce.entity.Enum.PaymentMethod;
import io.karhit.ecommerce.entity.Enum.ShipmentMethod;
import io.karhit.ecommerce.entity.OrderEntity;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class OrderMapper implements RowMapper<OrderEntity> {

  @Override
  public OrderEntity mapRow(ResultSet resultSet, int i) throws SQLException {
    return mapOrder(resultSet);
  }

  public static OrderEntity mapOrder(ResultSet resultSet) throws SQLException {
    return OrderEntity.builder()
        .id(resultSet.getLong("id"))
        .userId(resultSet.getLong("user_id"))
        .totalPrice(resultSet.getBigDecimal("total_price"))
        .receiverName(resultSet.getString("receiver_name"))
        .email(resultSet.getString("email"))
        .phone(resultSet.getString("phone"))
        .houseNumberStreet(resultSet.getString("house_number_street"))
        .address(resultSet.getString("address"))
        .status(OrderStatus.valueOf(resultSet.getString("status").toUpperCase()))
        .shipmentMethod(ShipmentMethod.valueOf(resultSet.getString("shipment_method").toUpperCase()))
        .paymentMethod(PaymentMethod.valueOf(resultSet.getString("payment_method").toUpperCase()))
        .createdAt(resultSet.getDate("created_at"))
        .build();
  }
}
