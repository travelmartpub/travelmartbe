package io.karhit.ecommerce.mapper;

import io.karhit.ecommerce.entity.ProductEntity;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ProductMapper implements RowMapper<ProductEntity> {

    @Override
    public ProductEntity mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        return mapProduct(resultSet);
    }

    public static ProductEntity mapProduct(ResultSet resultSet) throws SQLException {
        return ProductEntity.builder()
            .id(resultSet.getLong("id"))
            .storeId(resultSet.getLong("store_id"))
            .name(resultSet.getString("name"))
            .description(resultSet.getString("description"))
            .price(resultSet.getBigDecimal("price"))
            .stock(resultSet.getInt("stock"))
            .sold(resultSet.getInt("sold"))
            .createdAt(resultSet.getDate("created_at"))
            .images(resultSet.getString("images"))
            .activated(resultSet.getBoolean("activated"))
            .build();
    }
}
