package io.karhit.ecommerce.mapper;

import io.karhit.ecommerce.entity.StoreEntity;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class StoreMapper implements RowMapper<StoreEntity> {

    @Override
    public StoreEntity mapRow(ResultSet resultSet, int i) throws SQLException {
        return mapOrder(resultSet);
    }

    public static StoreEntity mapOrder(ResultSet resultSet) throws SQLException {
        return StoreEntity.builder()
            .id(resultSet.getLong("id"))
            .name(resultSet.getString("name"))
            .isActive(resultSet.getBoolean("activated"))
            .build();

    }
}
