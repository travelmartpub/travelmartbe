package io.karhit.ecommerce.mapper;

import io.karhit.ecommerce.entity.UserAuthorityEntity;
import io.karhit.ecommerce.entity.UserEntity;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class UserAuthorityMapper implements RowMapper<UserAuthorityEntity> {

  public UserAuthorityEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
    return mapUser(rs);
  }

  public static UserAuthorityEntity mapUser(ResultSet rs) throws SQLException {
    return UserAuthorityEntity.builder()
        .userId(rs.getLong("user_id"))
        .name(rs.getString("authority_name"))
        .build();
  }

}
