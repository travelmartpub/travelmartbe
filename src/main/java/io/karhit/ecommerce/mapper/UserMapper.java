package io.karhit.ecommerce.mapper;

import io.karhit.ecommerce.entity.UserEntity;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class UserMapper implements RowMapper<UserEntity> {

    public UserEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
        return mapUser(rs);
    }

    public static UserEntity mapUser(ResultSet rs) throws SQLException {
        return UserEntity.builder()
            .id(rs.getLong("id"))
            .userName(rs.getString("user_name"))
            .email((rs.getString("email")))
            .password(rs.getString("password_hash"))
            .activated(rs.getBoolean("activated"))
            .storeId(rs.getLong("store_id"))
            .avatar(rs.getString("avatar"))
            .build();
    }

}
