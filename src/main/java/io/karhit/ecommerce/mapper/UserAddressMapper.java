package io.karhit.ecommerce.mapper;

import io.karhit.ecommerce.entity.UserAddressEntity;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class UserAddressMapper implements RowMapper<UserAddressEntity> {

  @Override
  public UserAddressEntity mapRow(ResultSet resultSet, int i) throws SQLException {
    return mapUserAddress(resultSet);
  }

  public static UserAddressEntity mapUserAddress(ResultSet rs) throws SQLException {
    return UserAddressEntity.builder()
        .id(rs.getLong("id"))
        .username(rs.getString("username"))
        .phone(rs.getString("phone"))
        .houseNumberStreet(rs.getString("house_number_street"))
        .address(rs.getString("address"))
        .build();
  }
}
