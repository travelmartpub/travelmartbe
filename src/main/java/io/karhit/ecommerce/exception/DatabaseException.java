package io.karhit.ecommerce.exception;

public class DatabaseException extends Exception {

  public DatabaseException(String msg) {
    super(msg);
  }

  public DatabaseException(String msg, Exception ex) {
    super(msg, ex);
  }
}
