package io.karhit.ecommerce.exception;

public class ResourcesNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public ResourcesNotFoundException() {
        super();
    }

    public ResourcesNotFoundException(String exception) {
        super(exception);
    }

    public ResourcesNotFoundException(String format, Object... args) {
        super(String.format(format, args));
    }

}
