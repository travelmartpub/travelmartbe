package io.karhit.ecommerce.exception;

public class AccountResourceException extends RuntimeException {

    private AccountResourceException(String message) {
        super(message);
    }
}