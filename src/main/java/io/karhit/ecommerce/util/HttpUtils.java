package io.karhit.ecommerce.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HttpUtils {

  private static final Logger logger = LogManager.getLogger(HttpUtils.class);

  public static String sendPut(List<NameValuePair> params, String url, int timeout)
      throws Exception {
    logger.info("Send put url: {}, params: {}", url, params.toString());

    String result = null;
    RequestConfig defaultRequestConfig = RequestConfig.custom().setSocketTimeout(timeout)
        .setConnectTimeout(timeout).build();
    CloseableHttpClient httpClient = HttpClients.custom()
        .setDefaultRequestConfig(defaultRequestConfig).build();

    try {

      HttpPut httpPut = new HttpPut(url);
      httpPut.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));

      CloseableHttpResponse response = httpClient.execute(httpPut);
      if (response.getStatusLine().getStatusCode() != 200) {
        throw new IOException(
            "Failed : HTTP getStatusCode: " + response.getStatusLine().getStatusCode() +
                " HTTP getReasonPhrase: " + response.getStatusLine().getReasonPhrase());
      }

      HttpEntity entity = response.getEntity();
      InputStream inputStream = entity.getContent();
      try {
        result = IOUtils.toString(inputStream, "UTF-8");
      } finally {
        inputStream.close();
      }
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ParseException e) {
      e.printStackTrace();
    } finally {
      if (httpClient != null) {
        httpClient.close();
      }
    }

    return result;
  }

  public static String sendPost(List<NameValuePair> params, String url, int timeout)
      throws Exception {
    logger.info("Send post url: {}, params: {}", url, params.toString());

    String result = null;
    RequestConfig defaultRequestConfig = RequestConfig.custom().setSocketTimeout(timeout)
        .setConnectTimeout(timeout).build();
    CloseableHttpClient httpClient = HttpClients.custom()
        .setDefaultRequestConfig(defaultRequestConfig).build();

    try {

      HttpPost httpPost = new HttpPost(url);
      httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));

      CloseableHttpResponse response = httpClient.execute(httpPost);
      if (response.getStatusLine().getStatusCode() != 200) {
        throw new IOException(
            "Failed : HTTP getStatusCode: " + response.getStatusLine().getStatusCode() +
                " HTTP getReasonPhrase: " + response.getStatusLine().getReasonPhrase());
      }

      HttpEntity entity = response.getEntity();
      InputStream inputStream = entity.getContent();
      try {
        result = IOUtils.toString(inputStream, "UTF-8");
      } finally {
        inputStream.close();
      }
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ParseException e) {
      e.printStackTrace();
    } finally {
      if (httpClient != null) {
        httpClient.close();
      }
    }

    return result;
  }

  public static String sendPostJson(String json, String url) throws Exception {

    StringEntity entityJson = new StringEntity(json, "UTF-8");
    logger.info("Send post url={}, data={}", url, entityJson);

    String result = null;
    CloseableHttpClient httpClient = HttpClients.createDefault();

    try {

      HttpPost httpPost = new HttpPost(url);
      httpPost.setEntity(entityJson);
      httpPost.setHeader("Accept", "application/json");
      httpPost.setHeader("Content-type", "application/json");

      CloseableHttpResponse response = httpClient.execute(httpPost);
      if (response.getStatusLine().getStatusCode() != 200) {
//                throw new IOException("Failed : HTTP getStatusCode: " + response.getStatusLine().getStatusCode() + " HTTP getReasonPhrase: " + response.getStatusLine().getReasonPhrase());
        logger.info("Failed : HTTP getStatusCode: " + response.getStatusLine().getStatusCode()
            + " HTTP getReasonPhrase: " + response.getStatusLine().getReasonPhrase());
        return null;
      }

      HttpEntity entity = response.getEntity();
      InputStream inputStream = entity.getContent();
      try {
        result = IOUtils.toString(inputStream, "UTF-8");
      } finally {
        inputStream.close();
      }
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ParseException e) {
      e.printStackTrace();
    } finally {
      if (httpClient != null) {
        httpClient.close();
      }
    }

    return result;
  }

  public static String sendPostJson(String postUrl, String jsonContent, int timeout)
      throws Exception {
    RequestConfig defaultRequestConfig = RequestConfig.custom().setSocketTimeout(timeout)
        .setConnectTimeout(timeout).build();
    CloseableHttpClient httpClient = HttpClients.custom()
        .setDefaultRequestConfig(defaultRequestConfig).build();
    Throwable var5 = null;

    try {
      HttpPost httpPost = new HttpPost(postUrl);
      StringEntity input = new StringEntity(jsonContent, "UTF-8");
      input.setContentType("application/json");
      httpPost.setEntity(input);
      logger.info("Send post url={}, data={}", postUrl, jsonContent);
      CloseableHttpResponse response = httpClient.execute(httpPost);
      Throwable var9 = null;

      try {
        if (response.getStatusLine().getStatusCode() != 200) {
          throw new IOException(
              "Failed : HTTP getStatusCode: " + response.getStatusLine().getStatusCode()
                  + " HTTP getReasonPhrase: " + response.getStatusLine().getReasonPhrase());
        } else {
          BufferedReader br = new BufferedReader(
              new InputStreamReader(response.getEntity().getContent()));
          Throwable var11 = null;

          try {
            StringBuilder strBuilder = new StringBuilder();

            String output;
            while ((output = br.readLine()) != null) {
              strBuilder.append(output);
            }

            String var14 = strBuilder.toString();
            return var14;
          } catch (Throwable var58) {
            var11 = var58;
            throw var58;
          } finally {
            if (br != null) {
              if (var11 != null) {
                try {
                  br.close();
                } catch (Throwable var57) {
                  var11.addSuppressed(var57);
                }
              } else {
                br.close();
              }
            }

          }
        }
      } catch (Throwable var60) {
        var9 = var60;
        throw var60;
      } finally {
        if (response != null) {
          if (var9 != null) {
            try {
              response.close();
            } catch (Throwable var56) {
              var9.addSuppressed(var56);
            }
          } else {
            response.close();
          }
        }

      }
    } catch (Throwable var62) {
      var5 = var62;
      throw var62;
    } finally {
      if (httpClient != null) {
        if (var5 != null) {
          try {
            httpClient.close();
          } catch (Throwable var55) {
            var5.addSuppressed(var55);
          }
        } else {
          httpClient.close();
        }
      }

    }
  }

  public static String sendGet(String url, int timeout) throws IOException {
    RequestConfig defaultRequestConfig = RequestConfig.custom().setSocketTimeout(timeout)
        .setConnectTimeout(timeout).setConnectionRequestTimeout(timeout)
        .setStaleConnectionCheckEnabled(true).build();
    CloseableHttpClient httpclient = HttpClients.custom()
        .setDefaultRequestConfig(defaultRequestConfig).build();
    BufferedReader rd = null;
    CloseableHttpResponse response = null;

    try {
      HttpGet request = new HttpGet(url);
      logger.info("Send get url={}", url);
      response = httpclient.execute(request);
      rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
      StringBuilder result = new StringBuilder();

      String line;
      while ((line = rd.readLine()) != null) {
        result.append(line);
      }

      return result.toString();
    } catch (Throwable var60) {
      logger.info("Exception sendGet url={}", url);
      return null;
    } finally {
      if (rd != null) {
        rd.close();
      }

      if (response != null) {
        response.close();
      }

      if (httpclient != null) {
        httpclient.close();
      }

    }
  }
}
