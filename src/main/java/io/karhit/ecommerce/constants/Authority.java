package io.karhit.ecommerce.constants;

import lombok.Getter;

@Getter
public enum Authority {
  ROLE_ADMIN("ROLE_ADMIN"),
  ROLE_USER("ROLE_USER"),
  ROLE_STORE("ROLE_STORE");

  private final String value;

  Authority(String authority) {
    value = authority;
  }
}
