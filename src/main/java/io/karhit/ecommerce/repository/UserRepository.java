package io.karhit.ecommerce.repository;

import io.karhit.ecommerce.entity.UserEntity;
import io.karhit.ecommerce.exception.DatabaseException;
import java.util.List;
import java.util.Optional;

public interface UserRepository {

  List<UserEntity> getUsers(List<String> uIds);

  Optional<UserEntity> getUserByEmail(String userName);

  Optional<UserEntity> getUserByUserName(String lowercaseLogin);

  UserEntity save(UserEntity userEntity) throws DatabaseException;

  Optional<Long> getStoreId(String userName);

  Optional<Long> setStoreId(String username, Long storeId);
}
