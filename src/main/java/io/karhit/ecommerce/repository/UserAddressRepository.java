package io.karhit.ecommerce.repository;

import io.karhit.ecommerce.entity.UserAddressEntity;
import io.karhit.ecommerce.entity.request.UserAddressRequest;
import io.karhit.ecommerce.exception.DatabaseException;
import java.util.Optional;

public interface UserAddressRepository {

  Optional<UserAddressEntity> getUserAddresses(String username);

  UserAddressEntity save(UserAddressEntity userAddressEntity) throws DatabaseException;

  UserAddressRequest update(UserAddressRequest userAddressRequest, String username) throws DatabaseException;
}
