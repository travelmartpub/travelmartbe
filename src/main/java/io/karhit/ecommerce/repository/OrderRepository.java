package io.karhit.ecommerce.repository;

import io.karhit.ecommerce.entity.OrderEntity;
import io.karhit.ecommerce.exception.DatabaseException;
import java.util.Collection;
import java.util.Optional;

public interface OrderRepository {

  public Optional<OrderEntity> getOrderById(Long orderId);

  Collection<OrderEntity> getAllByUserId(Long userId, Integer limit, Integer offset);

  Collection<OrderEntity> getAllByStoreId(Long storeId, Integer limit, Integer offset);

  public OrderEntity save(OrderEntity orderEntity) throws DatabaseException;

  Integer getUserOrderCount(Long userId);

  Integer getStoreOrderCount(long storeId);
}
