package io.karhit.ecommerce.repository;

import javax.sql.DataSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public abstract class AbstractRepository {

  private final DataSource dataSource;
  private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

  protected AbstractRepository(DataSource dataSource) {
    this.dataSource = dataSource;
  }

  protected NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
    if (namedParameterJdbcTemplate == null)
      namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);

    return namedParameterJdbcTemplate;
  }
}
