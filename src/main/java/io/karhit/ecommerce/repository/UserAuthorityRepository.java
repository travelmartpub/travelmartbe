package io.karhit.ecommerce.repository;

import io.karhit.ecommerce.entity.UserAuthorityEntity;
import io.karhit.ecommerce.exception.DatabaseException;
import java.util.List;

public interface UserAuthorityRepository {

  List<UserAuthorityEntity> getAuthoritiesByUserId(Long id);

  UserAuthorityEntity save(UserAuthorityEntity userAuthorityEntity) throws DatabaseException;

}
