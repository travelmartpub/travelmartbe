package io.karhit.ecommerce.repository;

import io.karhit.ecommerce.entity.ProductEntity;
import io.karhit.ecommerce.entity.request.ProductRequest;
import io.karhit.ecommerce.exception.DatabaseException;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface ProductRepository {

  Integer getCount();

  Integer getStoreProductCount(Long storeId);

  Integer getStock(Long productId);

  Collection<ProductEntity> getPagingProducts(Integer limit, Integer offset, String orderBy);

  Collection<ProductEntity> getStorePagingProducts(Long storeId, Integer limit, Integer offset, String orderBy);

  List<ProductEntity> getAllByActive(Set<Long> pIds, boolean isActive);

  Optional<ProductEntity> getProductByIdAndActive(Long pId, boolean isActive);

  Optional<ProductEntity> getProductById(Long pId);

  Optional<Long> getStoreId(Long productId);

  ProductEntity save(ProductEntity productEntity) throws DatabaseException;

  Long update(ProductRequest productRequest) throws DatabaseException;

  Long subtractStock(Long productId, Integer quantity) throws DatabaseException;

  Long addStock(Long productId, Integer quantity) throws DatabaseException;

  Long deactivate(Long productId, boolean activated) throws DatabaseException;
}
