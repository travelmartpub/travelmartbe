package io.karhit.ecommerce.repository;

import io.karhit.ecommerce.entity.OrderProductDetailEntity;
import io.karhit.ecommerce.exception.DatabaseException;
import java.util.List;
import java.util.Optional;

public interface OrderProductDetailRepository {

  public Optional<OrderProductDetailEntity> getOrderProductDetailById(Long orderProductDetailId);

  List<OrderProductDetailEntity> getAllByOrderStoreDetailId(Long orderStoreDetailId);

  public OrderProductDetailEntity save(OrderProductDetailEntity orderProductDetail) throws DatabaseException;
}
