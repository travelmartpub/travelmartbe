package io.karhit.ecommerce.repository;

import io.karhit.ecommerce.entity.StoreEntity;
import io.karhit.ecommerce.exception.DatabaseException;
import java.util.Optional;

public interface StoreRepository {

    Optional<StoreEntity> getStoreById(Long storeId);

    Optional<StoreEntity> getStoreByIdAndActive(Long storeId, boolean isActive);

    StoreEntity save(StoreEntity storeEntity) throws DatabaseException;
}
