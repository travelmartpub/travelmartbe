package io.karhit.ecommerce.repository;

import io.karhit.ecommerce.entity.Enum.OrderStatus;
import io.karhit.ecommerce.entity.OrderStoreDetailEntity;
import io.karhit.ecommerce.exception.DatabaseException;
import java.util.Collection;
import java.util.Optional;

public interface OrderStoreDetailRepository {

  public Optional<OrderStoreDetailEntity> getOrderStoreDetailById(Long orderStoreDetailId);

  Collection<OrderStoreDetailEntity> getALLByOrderId(Long orderId);

  Collection<OrderStoreDetailEntity> getALLByOrderIdAndStoreId(Long orderId, Long storeId);

  Collection<OrderStoreDetailEntity> getOrderStoreDetailsByStoreId(Long storeId, Integer limit, Integer offset);

  Long updateStatus(OrderStatus orderStatus, Long orderStoreDetailId) throws DatabaseException;

  public OrderStoreDetailEntity save(OrderStoreDetailEntity orderStoreDetail) throws DatabaseException;

  Integer getOrderStoreDetailCount(Long storeId);
}
