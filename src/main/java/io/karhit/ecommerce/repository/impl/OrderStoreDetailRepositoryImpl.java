package io.karhit.ecommerce.repository.impl;

import io.karhit.ecommerce.entity.Enum.OrderStatus;
import io.karhit.ecommerce.entity.OrderStoreDetailEntity;
import io.karhit.ecommerce.exception.DatabaseException;
import io.karhit.ecommerce.mapper.OrderStoreDetailMapper;
import io.karhit.ecommerce.repository.AbstractRepository;
import io.karhit.ecommerce.repository.OrderStoreDetailRepository;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import javax.sql.DataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
@Slf4j
public class OrderStoreDetailRepositoryImpl extends AbstractRepository implements OrderStoreDetailRepository {

  private static final String QUERY_BY_ID = "SELECT * FROM order_store_detail WHERE id=:id LIMIT 1";

  private static final String QUERY_BY_STORE_ID = "SELECT * FROM order_store_detail WHERE store_id=:store_id LIMIT :limit OFFSET :offset";

  private static final String QUERY_COUNT_BY_STORE_ID = "SELECT COUNT(*) FROM order_store_detail WHERE store_id=:store_id";

  private static final String QUERY_BY_ORDER_ID = "SELECT * FROM order_store_detail WHERE order_id=:order_id";

  private static final String QUERY_BY_ORDER_ID_ABD_STORE_ID = "SELECT * FROM order_store_detail WHERE order_id=:order_id AND store_id=:store_id";

  private static final String UPDATE_STATUS = "UPDATE order_store_detail SET status=:status WHERE id=:id";

  private static final String UPSERT_ORDER_STORE_DETAIL =
      "INSERT INTO order_store_detail (id, order_id, store_id, store_name, total_price, status) "
          + "VALUES (:id, :order_id, :store_id, :store_name, :total_price, :status)";

  protected OrderStoreDetailRepositoryImpl(@Qualifier("ecommerceDataSource") DataSource dataSource) {
    super(dataSource);
  }

  @Override
  public Optional<OrderStoreDetailEntity> getOrderStoreDetailById(Long orderStoreDetailId) {
    try {
      MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
      sqlParameterSource.addValue("id", orderStoreDetailId);

      return Optional.ofNullable(
          getNamedParameterJdbcTemplate()
              .queryForObject(QUERY_BY_ID, sqlParameterSource,
                  new OrderStoreDetailMapper())
      );

    } catch (EmptyResultDataAccessException e) {
      return Optional.empty();
    }
  }

  @Override
  public Collection<OrderStoreDetailEntity> getALLByOrderId(Long orderId) {
    try {
      MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
      sqlParameterSource.addValue("order_id", orderId);

      return getNamedParameterJdbcTemplate().query(QUERY_BY_ORDER_ID, sqlParameterSource, new OrderStoreDetailMapper());

    } catch (EmptyResultDataAccessException e) {
      log.info("Result of getOrderStoreDetails was empty.");
      return Collections.emptyList();
    }
  }

  @Override
  public Collection<OrderStoreDetailEntity> getALLByOrderIdAndStoreId(Long orderId, Long storeId) {
    try {
      MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
      sqlParameterSource.addValue("order_id", orderId);
      sqlParameterSource.addValue("store_id", storeId);

      return getNamedParameterJdbcTemplate().query(QUERY_BY_ORDER_ID_ABD_STORE_ID, sqlParameterSource, new OrderStoreDetailMapper());

    } catch (EmptyResultDataAccessException e) {
      log.info("Result of getALLByOrderIdAndStoreId was empty.");
      return Collections.emptyList();
    }
  }

  @Override
  public Collection<OrderStoreDetailEntity> getOrderStoreDetailsByStoreId(Long storeId, Integer limit, Integer offset) {
    try {
      MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
      sqlParameterSource.addValue("store_id", storeId);
      sqlParameterSource.addValue("limit", limit);
      sqlParameterSource.addValue("offset", offset);

      return getNamedParameterJdbcTemplate()
          .query(QUERY_BY_STORE_ID, sqlParameterSource,
              new OrderStoreDetailMapper());

    } catch (EmptyResultDataAccessException e) {
      return Collections.emptyList();
    }
  }

  @Override
  public Long updateStatus(OrderStatus orderStatus, Long orderStoreDetailId) throws DatabaseException {
    MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
    sqlParameterSource.addValue("id", orderStoreDetailId);
    sqlParameterSource.addValue("status", orderStatus.getValue());

    int affectedRow = getNamedParameterJdbcTemplate().update(UPDATE_STATUS, sqlParameterSource);

    if (affectedRow == 0) {
      throw new DatabaseException("Deactivate fail for orderStoreDetail=" + orderStoreDetailId);
    }

    return orderStoreDetailId;
  }

  @Override
  public OrderStoreDetailEntity save(OrderStoreDetailEntity orderStoreDetail) throws DatabaseException {
    MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
    sqlParameterSource.addValue("id", orderStoreDetail.getId());
    sqlParameterSource.addValue("order_id", orderStoreDetail.getOrderId());
    sqlParameterSource.addValue("store_id", orderStoreDetail.getStoreId());
    sqlParameterSource.addValue("store_name", orderStoreDetail.getStoreName());
    sqlParameterSource.addValue("total_price", orderStoreDetail.getTotalPrice());
    sqlParameterSource.addValue("status", orderStoreDetail.getStatus().getValue());

    int affectedRow = getNamedParameterJdbcTemplate().update(UPSERT_ORDER_STORE_DETAIL, sqlParameterSource);

    if (affectedRow == 0) {
      throw new DatabaseException("Upsert fail for order_store_detail=" + orderStoreDetail.getId());
    }

    return orderStoreDetail;
  }

  @Override
  public Integer getOrderStoreDetailCount(Long storeId) {
    MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
    sqlParameterSource.addValue("store_id", storeId);
    return getNamedParameterJdbcTemplate().queryForObject(QUERY_COUNT_BY_STORE_ID, sqlParameterSource,
        Integer.class);
  }
}
