package io.karhit.ecommerce.repository.impl;

import io.karhit.ecommerce.entity.StoreEntity;
import io.karhit.ecommerce.exception.DatabaseException;
import io.karhit.ecommerce.mapper.StoreMapper;
import io.karhit.ecommerce.repository.AbstractRepository;
import io.karhit.ecommerce.repository.StoreRepository;
import java.util.Optional;
import javax.sql.DataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
@Slf4j
public class StoreRepositoryImpl extends AbstractRepository implements StoreRepository {

    private static final String QUERY_BY_ID = "SELECT * FROM store WHERE id =:id" +
                                                  " LIMIT 1";

    private static final String QUERY_BY_ID_AND_ACTIVE = "SELECT * FROM store WHERE id=:id AND activated=:activated" +
                                                             " LIMIT 1";

    private static final String UPSERT_STORE = "INSERT INTO store (id, name, activated) "
                                                   + "VALUES (:id, :name, :activated)";

    protected StoreRepositoryImpl(@Qualifier("ecommerceDataSource") DataSource dataSource) {
        super(dataSource);
    }

    @Override
    public Optional<StoreEntity> getStoreById(Long storeId) {
        try {
            MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
            sqlParameterSource.addValue("id", storeId);

            return Optional.ofNullable(
                getNamedParameterJdbcTemplate()
                    .queryForObject(QUERY_BY_ID, sqlParameterSource,
                        new StoreMapper())
            );

        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<StoreEntity> getStoreByIdAndActive(Long storeId, boolean isActive) {
        try {
            MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
            sqlParameterSource.addValue("id", storeId);
            sqlParameterSource.addValue("activated", isActive);

            return Optional.ofNullable(
                getNamedParameterJdbcTemplate()
                    .queryForObject(QUERY_BY_ID_AND_ACTIVE, sqlParameterSource,
                        new StoreMapper())
            );

        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public StoreEntity save(StoreEntity storeEntity) throws DatabaseException {
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("id", storeEntity.getId());
        sqlParameterSource.addValue("name", storeEntity.getName());
        sqlParameterSource.addValue("activated", true);

        int affectedRow = getNamedParameterJdbcTemplate().update(UPSERT_STORE, sqlParameterSource);

        if (affectedRow == 0) {
            throw new DatabaseException("Upsert fail for store=" + storeEntity.getId());
        }

        return storeEntity;
    }
}
