package io.karhit.ecommerce.repository.impl;

import io.karhit.ecommerce.entity.OrderEntity;
import io.karhit.ecommerce.exception.DatabaseException;
import io.karhit.ecommerce.mapper.OrderMapper;
import io.karhit.ecommerce.repository.AbstractRepository;
import io.karhit.ecommerce.repository.OrderRepository;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import javax.sql.DataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
@Slf4j
public class OrderRepositoryImpl extends AbstractRepository implements OrderRepository {

  private static final String QUERY_BY_ID = "SELECT * FROM `order` WHERE id=:id LIMIT 1";

  private static final String QUERY_BY_USER_ID = "SELECT * FROM `order` WHERE user_id=:user_id ORDER BY created_at DESC LIMIT :limit OFFSET :offset";

  private static final String QUERY_COUNT_BY_USER_ID = "SELECT COUNT(*) FROM `order` WHERE user_id=:user_id";

  private static final String QUERY_COUNT_BY_STORE_ID = "SELECT COUNT(*) from `order` o, order_store_detail osd "
      + "WHERE o.id = osd.order_id AND osd.store_id = :store_id";

  private static final String QUERY_BY_STORE_ID = "SELECT o.* from `order` o, order_store_detail osd "
      + "WHERE o.id = osd.order_id AND osd.store_id = :store_id LIMIT :limit OFFSET :offset";

  private static final String UPSERT_ORDER =
      "INSERT INTO `order` (id, user_id, total_price, receiver_name, phone, email, house_number_street, "
          + "address, status, shipment_method, payment_method, created_at) "
          + "VALUES (:id, :user_id, :total_price, :receiver_name, :phone, :email, :house_number_street, :address, "
          + ":status, :shipment_method, :payment_method, :created_at)";

  protected OrderRepositoryImpl(@Qualifier("ecommerceDataSource") DataSource dataSource) {
    super(dataSource);
  }

  @Override
  public Optional<OrderEntity> getOrderById(Long orderId) {
    try {
      MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
      sqlParameterSource.addValue("id", orderId);

      return Optional.ofNullable(
          getNamedParameterJdbcTemplate()
              .queryForObject(QUERY_BY_ID, sqlParameterSource,
                  new OrderMapper())
      );

    } catch (EmptyResultDataAccessException e) {
      return Optional.empty();
    }
  }

  @Override
  public Collection<OrderEntity> getAllByUserId(Long userId, Integer limit, Integer offset) {
    try {
      MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
      sqlParameterSource.addValue("user_id", userId);
      sqlParameterSource.addValue("limit", limit);
      sqlParameterSource.addValue("offset", offset);

      return getNamedParameterJdbcTemplate().query(QUERY_BY_USER_ID, sqlParameterSource, new OrderMapper());

    } catch (EmptyResultDataAccessException e) {
      log.info("Result of getAllByUserId was empty.");
      return Collections.emptyList();
    }
  }

  @Override
  public Collection<OrderEntity> getAllByStoreId(Long storeId, Integer limit, Integer offset) {
    try {
      MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
      sqlParameterSource.addValue("store_id", storeId);
      sqlParameterSource.addValue("limit", limit);
      sqlParameterSource.addValue("offset", offset);

      return getNamedParameterJdbcTemplate().query(QUERY_BY_STORE_ID, sqlParameterSource, new OrderMapper());

    } catch (EmptyResultDataAccessException e) {
      log.info("Result of getAllByStoreId was empty.");
      return Collections.emptyList();
    }
  }

  @Override
  public OrderEntity save(OrderEntity orderEntity) throws DatabaseException {
    MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
    sqlParameterSource.addValue("id", orderEntity.getId());
    sqlParameterSource.addValue("user_id", orderEntity.getUserId());
    sqlParameterSource.addValue("total_price", orderEntity.getTotalPrice());
    sqlParameterSource.addValue("receiver_name", orderEntity.getReceiverName());
    sqlParameterSource.addValue("phone", orderEntity.getPhone());
    sqlParameterSource.addValue("email", orderEntity.getEmail());
    sqlParameterSource.addValue("house_number_street", orderEntity.getHouseNumberStreet());
    sqlParameterSource.addValue("address", orderEntity.getAddress());
    sqlParameterSource.addValue("status", orderEntity.getStatus().getValue());
    sqlParameterSource.addValue("shipment_method", orderEntity.getShipmentMethod().getValue());
    sqlParameterSource.addValue("payment_method", orderEntity.getPaymentMethod().getValue());
    sqlParameterSource.addValue("created_at", orderEntity.getCreatedAt());

    int affectedRow = getNamedParameterJdbcTemplate().update(UPSERT_ORDER, sqlParameterSource);

    if (affectedRow == 0) {
      throw new DatabaseException("Upsert fail for order=" + orderEntity.getId());
    }

    return orderEntity;
  }

  @Override
  public Integer getUserOrderCount(Long userId) {
    MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
    sqlParameterSource.addValue("user_id", userId);
    return getNamedParameterJdbcTemplate().queryForObject(QUERY_COUNT_BY_USER_ID, sqlParameterSource,
        Integer.class);
  }

  @Override
  public Integer getStoreOrderCount(long storeId) {
    MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
    sqlParameterSource.addValue("store_id", storeId);
    return getNamedParameterJdbcTemplate().queryForObject(QUERY_COUNT_BY_STORE_ID, sqlParameterSource,
        Integer.class);
  }
}
