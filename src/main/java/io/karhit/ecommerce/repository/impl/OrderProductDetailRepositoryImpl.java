package io.karhit.ecommerce.repository.impl;

import io.karhit.ecommerce.entity.OrderProductDetailEntity;
import io.karhit.ecommerce.exception.DatabaseException;
import io.karhit.ecommerce.mapper.OrderProductDetailMapper;
import io.karhit.ecommerce.repository.AbstractRepository;
import io.karhit.ecommerce.repository.OrderProductDetailRepository;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import javax.sql.DataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
@Slf4j
public class OrderProductDetailRepositoryImpl extends AbstractRepository implements OrderProductDetailRepository {

  private static final String QUERY_BY_ID = "SELECT * FROM order_product_detail WHERE id=:id LIMIT 1";

  private static final String QUERY_BY_STORE_ID = "SELECT * FROM order_product_detail WHERE order_store_detail_id=:order_store_detail_id";

  private static final String UPSERT_ORDER_PRODUCT_DETAIL =
      "INSERT INTO order_product_detail (id, order_store_detail_id, product_id, product_name, quantity, price, total_price) "
          + "VALUES (:id, :order_store_detail_id, :product_id, :product_name, :quantity, :price, :total_price)";

  protected OrderProductDetailRepositoryImpl(DataSource dataSource) {
    super(dataSource);
  }

  @Override
  public Optional<OrderProductDetailEntity> getOrderProductDetailById(Long orderProductDetailId) {
    try {
      MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
      sqlParameterSource.addValue("id", orderProductDetailId);

      return Optional.ofNullable(
          getNamedParameterJdbcTemplate()
              .queryForObject(QUERY_BY_ID, sqlParameterSource,
                  new OrderProductDetailMapper())
      );

    } catch (EmptyResultDataAccessException e) {
      return Optional.empty();
    }
  }

  @Override
  public List<OrderProductDetailEntity> getAllByOrderStoreDetailId(Long orderStoreDetailId) {
    try {
      MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
      sqlParameterSource.addValue("order_store_detail_id", orderStoreDetailId);
      return getNamedParameterJdbcTemplate().query(QUERY_BY_STORE_ID, sqlParameterSource, new OrderProductDetailMapper());

    } catch (EmptyResultDataAccessException e) {
      log.info("Result of getAllByOrderStoreDetailId was empty, orderStoreDetailId={}", orderStoreDetailId);
      return Collections.emptyList();
    }
  }

  @Override
  public OrderProductDetailEntity save(OrderProductDetailEntity orderProductDetail) throws DatabaseException {
    MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
    sqlParameterSource.addValue("id", orderProductDetail.getId());
    sqlParameterSource.addValue("order_store_detail_id", orderProductDetail.getOrderStoreDetailId());
    sqlParameterSource.addValue("product_id", orderProductDetail.getProductId());
    sqlParameterSource.addValue("product_name", orderProductDetail.getProductName());
    sqlParameterSource.addValue("quantity", orderProductDetail.getQuantity());
    sqlParameterSource.addValue("price", orderProductDetail.getPrice());
    sqlParameterSource.addValue("total_price", orderProductDetail.getTotalPrice());

    int affectedRow = getNamedParameterJdbcTemplate().update(UPSERT_ORDER_PRODUCT_DETAIL, sqlParameterSource);

    if (affectedRow == 0) {
      throw new DatabaseException("Upsert fail for order_product_detail=" + orderProductDetail.getId());
    }

    return orderProductDetail;
  }
}
