package io.karhit.ecommerce.repository.impl;

import io.karhit.ecommerce.entity.UserAddressEntity;
import io.karhit.ecommerce.entity.request.UserAddressRequest;
import io.karhit.ecommerce.exception.DatabaseException;
import io.karhit.ecommerce.mapper.UserAddressMapper;
import io.karhit.ecommerce.repository.AbstractRepository;
import io.karhit.ecommerce.repository.UserAddressRepository;
import java.util.Optional;
import javax.sql.DataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
@Slf4j
public class UserAddressRepositoryImpl extends AbstractRepository implements UserAddressRepository {

  private static final String QUERY_BY_USERNAME = "SELECT * FROM user_address WHERE username = :username LIMIT 1";

  private static final String UPSERT_USER_ADDRESS = "INSERT INTO user_address (id, username, phone, house_number_street, address) "
      + "VALUE (:id, :username, :phone, :house_number_street, :address)";

  private static final String UPDATE_USER_ADDRESS = "UPDATE user_address SET phone = :phone, house_number_street = :house_number_street, "
      + "address = :address WHERE username = :username";

  protected UserAddressRepositoryImpl(@Qualifier("ecommerceDataSource") DataSource dataSource) {
    super(dataSource);
  }

  @Override
  public Optional<UserAddressEntity> getUserAddresses(String username) {
    try {
      MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
      sqlParameterSource.addValue("username", username);

      return Optional.ofNullable(
          getNamedParameterJdbcTemplate()
              .queryForObject(QUERY_BY_USERNAME, sqlParameterSource,
                  new UserAddressMapper())
      );

    } catch (EmptyResultDataAccessException e) {
      return Optional.empty();
    }
  }

  @Override
  public UserAddressEntity save(UserAddressEntity userAddressEntity) throws DatabaseException {
    MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
    sqlParameterSource.addValue("id", userAddressEntity.getId());
    sqlParameterSource.addValue("username", userAddressEntity.getUsername());
    sqlParameterSource.addValue("phone", userAddressEntity.getPhone());
    sqlParameterSource.addValue("house_number_street", userAddressEntity.getHouseNumberStreet());
    sqlParameterSource.addValue("address", userAddressEntity.getAddress());

    int affectedRow = getNamedParameterJdbcTemplate().update(UPSERT_USER_ADDRESS, sqlParameterSource);

    if (affectedRow == 0) {
      throw new DatabaseException("Upsert fail for user address =" + userAddressEntity.getId());
    }

    return userAddressEntity;
  }

  @Override
  public UserAddressRequest update(UserAddressRequest userAddressRequest, String username) throws DatabaseException {
    MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
    sqlParameterSource.addValue("username", username);
    sqlParameterSource.addValue("phone", userAddressRequest.getPhone());
    sqlParameterSource.addValue("house_number_street", userAddressRequest.getHouseNumberStreet());
    sqlParameterSource.addValue("address", userAddressRequest.getAddress());

    int affectedRow = getNamedParameterJdbcTemplate().update(UPDATE_USER_ADDRESS, sqlParameterSource);

    if (affectedRow == 0) {
      throw new DatabaseException("Update fail for user address = " + userAddressRequest);
    }

    return userAddressRequest;
  }
}
