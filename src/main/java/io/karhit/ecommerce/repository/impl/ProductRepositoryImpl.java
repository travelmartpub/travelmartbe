package io.karhit.ecommerce.repository.impl;

import com.google.common.base.Strings;
import io.karhit.ecommerce.entity.ProductEntity;
import io.karhit.ecommerce.entity.request.ProductRequest;
import io.karhit.ecommerce.exception.DatabaseException;
import io.karhit.ecommerce.mapper.ProductMapper;
import io.karhit.ecommerce.repository.AbstractRepository;
import io.karhit.ecommerce.repository.ProductRepository;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.sql.DataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
@Slf4j
public class ProductRepositoryImpl extends AbstractRepository implements ProductRepository {

  private static final String QUERY_PRODUCT = "SELECT * FROM product WHERE id IN (:ids) AND activated=:activated " +
      "limit 20";

  private static final String QUERY_BY_ID_AND_ACTIVE = "SELECT * FROM product WHERE id=:id AND activated=:activated" +
      " LIMIT 1";

  private static final String QUERY_BY_ID = "SELECT * FROM product WHERE id=:id LIMIT 1";

  private static final String QUERY_STORE_ID = "SELECT store_id FROM product WHERE id=:id LIMIT 1";

  private static final String QUERY_PRODUCT_PAGING = "SELECT * FROM product WHERE activated=true LIMIT :limit OFFSET :offset";

  private static final String QUERY_STORE_PRODUCT_PAGING = "SELECT * FROM product WHERE store_id=:store_id AND activated=true LIMIT "
      + ":limit OFFSET :offset";

  private static final String QUERY_COUNT = "SELECT COUNT(*) FROM product WHERE activated=true";

  private static final String QUERY_COUNT_BY_STORE = "SELECT COUNT(*) FROM product WHERE store_id=:store_id";

  private static final String QUERY_STOCK_BY_ID = "SELECT stock FROM product WHERE id=:id";

  private static final String UPSERT_PRODUCT =
      "INSERT INTO product (id, name, store_id, description, price, stock, sold, images, created_at, activated) "
          + "VALUES (:id, :name, :store_id, :description, :price, :stock, :sold, :images, :created_at, :activated)";

  private static final String UPDATE_PRODUCT = "UPDATE product "
      + "SET name= :name, description= :description, price=:price, "
      + "stock=:stock, images=:images "
      + "WHERE id=:id";

  private static final String SUBTRACT_STOCK = "UPDATE product SET stock=stock - :quantity WHERE id=:id";

  private static final String ADD_STOCK = "UPDATE product SET stock=stock + :quantity WHERE id=:id";

  private static final String SET_ACTIVATE_PRODUCT = "UPDATE product SET activated = :activated WHERE id=:id";

  protected ProductRepositoryImpl(@Qualifier("ecommerceDataSource") DataSource dataSource) {
    super(dataSource);
  }

  @Override
  public Integer getCount() {
    MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
    return getNamedParameterJdbcTemplate().queryForObject(QUERY_COUNT, sqlParameterSource,
        Integer.class);
  }

  @Override
  public Integer getStoreProductCount(Long storeId) {
    MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
    sqlParameterSource.addValue("store_id", storeId);
    return getNamedParameterJdbcTemplate().queryForObject(QUERY_COUNT_BY_STORE, sqlParameterSource,
        Integer.class);
  }

  @Override
  public Integer getStock(Long productId) {
    MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
    sqlParameterSource.addValue("id", productId);
    return getNamedParameterJdbcTemplate().queryForObject(QUERY_STOCK_BY_ID, sqlParameterSource,
        Integer.class);
  }

  @Override
  public Collection<ProductEntity> getPagingProducts(Integer limit, Integer offset, String orderBy) {
    try {
      MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();

      String query = QUERY_PRODUCT_PAGING;

      if (!Strings.isNullOrEmpty(orderBy)) {
        query = "SELECT * FROM product WHERE activated=true ORDER BY " + orderBy + " LIMIT :limit OFFSET :offset";
      }

      sqlParameterSource.addValue("limit", limit);
      sqlParameterSource.addValue("offset", offset);
      return getNamedParameterJdbcTemplate().query(query, sqlParameterSource, new ProductMapper());

    } catch (EmptyResultDataAccessException e) {
      log.info("Result of getPagingProducts was empty.");
      return Collections.emptyList();
    }
  }

  @Override
  public Collection<ProductEntity> getStorePagingProducts(Long storeId, Integer limit, Integer offset, String orderBy) {
    try {
      MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();

      String query = QUERY_STORE_PRODUCT_PAGING;

      if (!Strings.isNullOrEmpty(orderBy)) {
        query = "SELECT * FROM product WHERE store_id=:store_id ORDER BY " + orderBy + " LIMIT :limit OFFSET :offset";
      }

      sqlParameterSource.addValue("store_id", storeId);
      sqlParameterSource.addValue("limit", limit);
      sqlParameterSource.addValue("offset", offset);
      return getNamedParameterJdbcTemplate().query(query, sqlParameterSource, new ProductMapper());

    } catch (EmptyResultDataAccessException e) {
      log.info("Result of getPagingProducts was empty.");
      return Collections.emptyList();
    }
  }

  @Override
  public List<ProductEntity> getAllByActive(Set<Long> pIds, boolean isActive) {
    try {
      MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
      sqlParameterSource.addValue("ids", pIds);
      sqlParameterSource.addValue("activated", isActive);
      return getNamedParameterJdbcTemplate().query(QUERY_PRODUCT, sqlParameterSource, new ProductMapper());

    } catch (EmptyResultDataAccessException e) {
      log.info("Result of getProducts was empty, pIds={}", pIds.stream().limit(20).collect(Collectors.toSet()));
      return Collections.emptyList();
    }
  }

  @Override
  public Optional<ProductEntity> getProductByIdAndActive(Long pId, boolean isActive) {
    try {
      MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
      sqlParameterSource.addValue("id", pId);
      sqlParameterSource.addValue("activated", isActive);

      return Optional.ofNullable(
          getNamedParameterJdbcTemplate()
              .queryForObject(QUERY_BY_ID_AND_ACTIVE, sqlParameterSource,
                  new ProductMapper())
      );

    } catch (EmptyResultDataAccessException e) {
      return Optional.empty();
    }
  }

  @Override
  public Optional<ProductEntity> getProductById(Long pId) {
    try {
      MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
      sqlParameterSource.addValue("id", pId);

      return Optional.ofNullable(
          getNamedParameterJdbcTemplate()
              .queryForObject(QUERY_BY_ID, sqlParameterSource,
                  new ProductMapper())
      );

    } catch (EmptyResultDataAccessException e) {
      return Optional.empty();
    }
  }

  @Override
  public Optional<Long> getStoreId(Long productId) {
    try {
      MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
      sqlParameterSource.addValue("id", productId);

      return Optional.ofNullable(
          getNamedParameterJdbcTemplate()
              .queryForObject(QUERY_STORE_ID, sqlParameterSource, Long.class)
      );

    } catch (EmptyResultDataAccessException e) {
      return Optional.empty();
    }
  }

  @Override
  public ProductEntity save(ProductEntity productEntity) throws DatabaseException {
    MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
    sqlParameterSource.addValue("id", productEntity.getId());
    sqlParameterSource.addValue("name", productEntity.getName());
    sqlParameterSource.addValue("store_id", productEntity.getStoreId());
    sqlParameterSource.addValue("description", productEntity.getDescription());
    sqlParameterSource.addValue("price", productEntity.getPrice());
    sqlParameterSource.addValue("stock", productEntity.getStock());
    sqlParameterSource.addValue("sold", productEntity.getSold());
    sqlParameterSource.addValue("images", productEntity.getImages());
    sqlParameterSource.addValue("created_at", productEntity.getCreatedAt());
    sqlParameterSource.addValue("activated", true);

    int affectedRow = getNamedParameterJdbcTemplate().update(UPSERT_PRODUCT, sqlParameterSource);

    if (affectedRow == 0) {
      throw new DatabaseException("Upsert fail for product=" + productEntity.getId());
    }

    return productEntity;
  }

  @Override
  public Long update(ProductRequest productRequest) throws DatabaseException {
    MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
    sqlParameterSource.addValue("id", productRequest.getId());
    sqlParameterSource.addValue("name", productRequest.getName());
    sqlParameterSource.addValue("description", productRequest.getDescription());
    sqlParameterSource.addValue("price", productRequest.getPrice());
    sqlParameterSource.addValue("stock", productRequest.getStock());
    sqlParameterSource.addValue("images", productRequest.getImages());

    int affectedRow = getNamedParameterJdbcTemplate().update(UPDATE_PRODUCT, sqlParameterSource);

    if (affectedRow == 0) {
      throw new DatabaseException("Update fail for product=" + productRequest.getId());
    }

    return productRequest.getId();
  }

  @Override
  public Long subtractStock(Long productId, Integer quantity) throws DatabaseException {
    MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
    sqlParameterSource.addValue("id", productId);
    sqlParameterSource.addValue("quantity", quantity);

    int affectedRow = getNamedParameterJdbcTemplate().update(SUBTRACT_STOCK, sqlParameterSource);

    if (affectedRow == 0) {
      throw new DatabaseException("Subtract Stock fail for product=" + productId);
    }

    return productId;
  }

  @Override
  public Long addStock(Long productId, Integer quantity) throws DatabaseException {
    MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
    sqlParameterSource.addValue("id", productId);
    sqlParameterSource.addValue("quantity", quantity);

    int affectedRow = getNamedParameterJdbcTemplate().update(ADD_STOCK, sqlParameterSource);

    if (affectedRow == 0) {
      throw new DatabaseException("Add Stock fail for product=" + productId);
    }

    return productId;
  }

  @Override
  public Long deactivate(Long productId, boolean activated) throws DatabaseException {
    MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
    sqlParameterSource.addValue("id", productId);
    sqlParameterSource.addValue("activated", activated);

    int affectedRow = getNamedParameterJdbcTemplate().update(SET_ACTIVATE_PRODUCT, sqlParameterSource);

    if (affectedRow == 0) {
      throw new DatabaseException("Set activated fail for product=" + productId);
    }

    return productId;
  }
}
