package io.karhit.ecommerce.repository.impl;

import io.karhit.ecommerce.entity.UserEntity;
import io.karhit.ecommerce.exception.DatabaseException;
import io.karhit.ecommerce.mapper.UserMapper;
import io.karhit.ecommerce.repository.AbstractRepository;
import io.karhit.ecommerce.repository.UserRepository;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import javax.sql.DataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;


@Repository
@Slf4j
public class UserRepositoryImpl extends AbstractRepository implements UserRepository {

    private static final String QUERY_USER = "SELECT * FROM user limit 200";

    private static final String QUERY_BY_USER_NAME = "SELECT * FROM user WHERE user_name=:user_name LIMIT 1";
    private static final String QUERY_BY_EMAIL = "SELECT id, email, user_name, activated FROM user WHERE email=:email"
        + " LIMIT 1";

    private static final String UPSERT_USER =
        "INSERT INTO user (id, email, user_name, password_hash, activated) "
            + "VALUES (:id, :email, :user_name, :password_hash, :is_active)";

    private static final String QUERY_STORE_ID = "SELECT store_id FROM user WHERE user_name = :user_name";

    private static final String UPDATE_STORE_ID = "UPDATE user SET store_id=:store_id WHERE user_name=:user_name";

    public UserRepositoryImpl(@Qualifier("ecommerceDataSource") DataSource dataSource) {
        super(dataSource);
    }

    @Override
    public List<UserEntity> getUsers(List<String> uIds) {

        try {
            return getNamedParameterJdbcTemplate().query(QUERY_USER, new UserMapper());

        } catch (EmptyResultDataAccessException e) {
            log.info("Result of getUsers was empty, uIds={}", uIds.subList(0, 20));
            return Collections.emptyList();
        }
    }

    @Override
    public Optional<UserEntity> getUserByEmail(String email) {
        try {
            MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
            sqlParameterSource.addValue("email", email);

            return Optional.ofNullable(
                getNamedParameterJdbcTemplate()
                    .queryForObject(QUERY_BY_EMAIL, sqlParameterSource,
                        new BeanPropertyRowMapper<>(UserEntity.class))
            );

        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<UserEntity> getUserByUserName(String userName) {
        try {
            MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
            sqlParameterSource.addValue("user_name", userName);

            return Optional.ofNullable(
                getNamedParameterJdbcTemplate()
                    .queryForObject(QUERY_BY_USER_NAME, sqlParameterSource, new UserMapper())
            );

        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public UserEntity save(UserEntity userEntity) throws DatabaseException {
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("id", userEntity.getId());
        sqlParameterSource.addValue("email", userEntity.getEmail());
        sqlParameterSource.addValue("user_name", userEntity.getUserName());
        sqlParameterSource.addValue("password_hash", userEntity.getPassword());
        sqlParameterSource.addValue("is_active", true);

        int affectedRow = getNamedParameterJdbcTemplate().update(UPSERT_USER, sqlParameterSource);

        if (affectedRow == 0) {
            throw new DatabaseException("Upsert fail for user=" + userEntity.getId());
        }

        return userEntity;
    }

    @Override
    public Optional<Long> getStoreId(String userName) {
        try {
            MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
            sqlParameterSource.addValue("user_name", userName);

            return Optional.ofNullable(
                getNamedParameterJdbcTemplate()
                    .queryForObject(QUERY_STORE_ID, sqlParameterSource, Long.class)
            );

        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<Long> setStoreId(String username, Long storeId) {
        try {
            MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
            sqlParameterSource.addValue("user_name", username);
            sqlParameterSource.addValue("store_id", storeId);

            getNamedParameterJdbcTemplate().update(UPDATE_STORE_ID, sqlParameterSource);

            return Optional.of(storeId);

        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

}
