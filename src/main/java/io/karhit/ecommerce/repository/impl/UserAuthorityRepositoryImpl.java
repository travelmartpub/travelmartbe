package io.karhit.ecommerce.repository.impl;

import io.karhit.ecommerce.entity.UserAuthorityEntity;
import io.karhit.ecommerce.exception.DatabaseException;
import io.karhit.ecommerce.mapper.UserAuthorityMapper;
import io.karhit.ecommerce.repository.AbstractRepository;
import io.karhit.ecommerce.repository.UserAuthorityRepository;
import java.util.Collections;
import java.util.List;
import javax.sql.DataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;


@Repository
@Slf4j
public class UserAuthorityRepositoryImpl extends AbstractRepository implements
    UserAuthorityRepository {

  private static final String QUERY_USER = "SELECT * FROM user_authority WHERE user_id=:user_id";
  private static final String UPSERT_USER_AUTHORITY =
      "INSERT INTO user_authority (user_id, authority_name) VALUES (:user_id, :authority_name)";

  public UserAuthorityRepositoryImpl(@Qualifier("ecommerceDataSource") DataSource dataSource) {
    super(dataSource);
  }

  @Override
  public List<UserAuthorityEntity> getAuthoritiesByUserId(Long userId) {
    try {
      MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
      sqlParameterSource.addValue("user_id", userId);

      return getNamedParameterJdbcTemplate()
          .query(QUERY_USER, sqlParameterSource, new UserAuthorityMapper());

    } catch (EmptyResultDataAccessException e) {
      return Collections.emptyList();
    }
  }

  @Override
  public UserAuthorityEntity save(UserAuthorityEntity userAuthorityEntity)
      throws DatabaseException {
    MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
    sqlParameterSource.addValue("user_id", userAuthorityEntity.getUserId());
    sqlParameterSource.addValue("authority_name", userAuthorityEntity.getName());

    int affectedRow = getNamedParameterJdbcTemplate()
        .update(UPSERT_USER_AUTHORITY, sqlParameterSource);

    if (affectedRow == 0) {
      throw new DatabaseException("Upsert fail for user authority=" + userAuthorityEntity);
    }

    return userAuthorityEntity;
  }
}
