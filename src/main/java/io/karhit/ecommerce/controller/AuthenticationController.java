package io.karhit.ecommerce.controller;

import static io.karhit.ecommerce.entity.response.ErrorConstants.REGISTER_FAIL;

import io.karhit.ecommerce.entity.UserEntity;
import io.karhit.ecommerce.entity.request.LoginReq;
import io.karhit.ecommerce.entity.request.RegisterRequest;
import io.karhit.ecommerce.entity.response.ErrorResponse;
import io.karhit.ecommerce.entity.response.ErrorResponse.ErrorObject;
import io.karhit.ecommerce.entity.response.WrapperResponseEntity;
import io.karhit.ecommerce.security.jwt.TokenProvider;
import io.karhit.ecommerce.service.AuthService;
import java.util.Collections;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller to authenticate users.
 */
@RestController
@RequestMapping("/auth")
@Slf4j
public class AuthenticationController {

    private final TokenProvider tokenProvider;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final AuthService authService;

    public AuthenticationController(
        TokenProvider tokenProvider, AuthenticationManagerBuilder authenticationManagerBuilder,
        AuthService authService) {
        this.tokenProvider = tokenProvider;
        this.authenticationManagerBuilder = authenticationManagerBuilder;
        this.authService = authService;
    }

    @PostMapping("/login")
    public ResponseEntity<WrapperResponseEntity> login(@RequestBody LoginReq loginReq) {
        log.info("Start auth user {}", loginReq);

        UsernamePasswordAuthenticationToken authenticationToken =
            new UsernamePasswordAuthenticationToken(loginReq.getUsername(), loginReq.getPassword());

        Authentication authentication = authenticationManagerBuilder.getObject()
            .authenticate(authenticationToken);

        boolean rememberMe = loginReq.getRememberMe() != null && loginReq.getRememberMe();
        String jwt = tokenProvider.createToken(authentication, rememberMe);

        log.info("End auth user {}", loginReq.getUsername());
        return new ResponseEntity<>(new WrapperResponseEntity(new JWTToken(jwt)), HttpStatus.OK);
    }

    @PostMapping("/register-admin")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<WrapperResponseEntity> createAccount(
        @RequestParam String username,
        @RequestParam String email,
        @RequestParam String password
    ) {
        try {
            UserEntity userEntity = authService.register(username, email, password);
            return new ResponseEntity<>(new WrapperResponseEntity(userEntity), HttpStatus.OK);

        } catch (Exception e) {
            log.error("Register fail", e.getCause());
            return new ResponseEntity<>(
                new WrapperResponseEntity(new ErrorObject(REGISTER_FAIL)), HttpStatus.BAD_REQUEST);
        }

    }

    @PostMapping("/register")
    public ResponseEntity<WrapperResponseEntity> register(@RequestBody RegisterRequest registerRequest) {
        try {
            UserEntity userEntity = authService.register(registerRequest.getUsername(), registerRequest.getEmail(),
                registerRequest.getPassword());
            return new ResponseEntity<>(new WrapperResponseEntity(userEntity), HttpStatus.OK);

        } catch (Exception e) {
            log.error("Register fail", e.getCause().getLocalizedMessage());
            return new ResponseEntity<>(
                new WrapperResponseEntity(
                    new ErrorResponse.ErrorObject(
                        REGISTER_FAIL, Collections.singletonList(e.getCause().getLocalizedMessage()))),
                HttpStatus.OK);
        }

    }

    /**
     * Object to return as body in JWT Authentication.
     */
    @Setter
    @Getter
    static class JWTToken {

        private String token;

        JWTToken(String token) {
            this.token = token;
        }
    }
}
