package io.karhit.ecommerce.controller;

import io.karhit.ecommerce.entity.Enum.OrderStatus;
import io.karhit.ecommerce.entity.OrderStoreDetailEntity;
import io.karhit.ecommerce.entity.ProductEntity;
import io.karhit.ecommerce.entity.StoreEntity;
import io.karhit.ecommerce.entity.request.CreateStoreRequest;
import io.karhit.ecommerce.entity.request.ProductRequest;
import io.karhit.ecommerce.entity.response.ErrorConstants;
import io.karhit.ecommerce.entity.response.ErrorResponse;
import io.karhit.ecommerce.entity.response.PagingResponse;
import io.karhit.ecommerce.entity.response.WrapperResponseEntity;
import io.karhit.ecommerce.service.StoreService;
import java.util.Collection;
import java.util.Collections;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/stores")
@Slf4j
public class StoreController {

    private final StoreService storeService;

    public StoreController(StoreService storeService) {
        this.storeService = storeService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<WrapperResponseEntity> getStore(@PathVariable Long id) {
        log.info("Start getStore id={}", id);

        try {
            StoreEntity store = storeService.getById(id);
            log.info("End getStore id={}", id);

            return new ResponseEntity<>(new WrapperResponseEntity(store), HttpStatus.OK);
        } catch (Exception e) {
            log.error("#Error getStore, error={}", e.getLocalizedMessage());
            return new ResponseEntity<>(
                new WrapperResponseEntity(
                    new ErrorResponse.ErrorObject(
                        ErrorConstants.FAILURE, Collections.singletonList(e.getLocalizedMessage()))),
                HttpStatus.OK);
        }
    }

    @GetMapping("/activated/{id}")
    public ResponseEntity<WrapperResponseEntity> getActivatedStore(@PathVariable Long id) {
        log.info("Start getActivatedStore id={}", id);

        try {
            StoreEntity store = storeService.getActivatedById(id);
            log.info("End getActivatedStore id={}", id);

            return new ResponseEntity<>(new WrapperResponseEntity(store), HttpStatus.OK);
        } catch (Exception e) {
            log.error("#Error getActivatedStore, error={}", e.getLocalizedMessage());
            return new ResponseEntity<>(
                new WrapperResponseEntity(
                    new ErrorResponse.ErrorObject(
                        ErrorConstants.FAILURE, Collections.singletonList(e.getLocalizedMessage()))),
                HttpStatus.OK);
        }
    }

    @PostMapping("/create")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<WrapperResponseEntity> createStore(@RequestBody CreateStoreRequest createStoreRequest) {
        log.info("Start createStore createStoreRequest={}", createStoreRequest);

        try {
            StoreEntity store = storeService.save(createStoreRequest);
            log.info("End createStore createStoreRequest={}", createStoreRequest);

            return new ResponseEntity<>(new WrapperResponseEntity(store), HttpStatus.OK);
        } catch (Exception e) {
            log.error("#Error createStore, error={}", e.getLocalizedMessage());
            return new ResponseEntity<>(
                new WrapperResponseEntity(
                    new ErrorResponse.ErrorObject(
                        ErrorConstants.FAILURE, Collections.singletonList(e.getLocalizedMessage()))),
                HttpStatus.OK);
        }
    }

    @PostMapping("/user/create")
    public ResponseEntity<WrapperResponseEntity> createUserStore(@RequestBody CreateStoreRequest createStoreRequest) {
        log.info("Start createUserStore createStoreRequest={}", createStoreRequest);

        try {
            StoreEntity store = storeService.createUserStore(createStoreRequest);
            log.info("End createUserStore createStoreRequest={}", createStoreRequest);

            return new ResponseEntity<>(new WrapperResponseEntity(store), HttpStatus.OK);
        } catch (Exception e) {
            log.error("#Error createUserStore, error={}", e.getLocalizedMessage());
            return new ResponseEntity<>(
                new WrapperResponseEntity(
                    new ErrorResponse.ErrorObject(
                        ErrorConstants.FAILURE, Collections.singletonList(e.getLocalizedMessage()))),
                HttpStatus.OK);
        }
    }

    @PostMapping("/product/create")
    @PreAuthorize("hasRole('ROLE_STORE')")
    public ResponseEntity<WrapperResponseEntity> createProduct(@RequestBody ProductRequest productRequest) {
        log.info("Start createProduct ProductRequest={}", productRequest);

        try {
            ProductEntity productEntity = storeService.createProduct(productRequest);
            log.info("End createProduct ProductRequest={}", productRequest);

            return new ResponseEntity<>(new WrapperResponseEntity(productEntity), HttpStatus.OK);
        } catch (Exception e) {
            log.error("#Error createProduct, error={}", e.getLocalizedMessage());
            return new ResponseEntity<>(
              new WrapperResponseEntity(
                new ErrorResponse.ErrorObject(
                  ErrorConstants.FAILURE, Collections.singletonList(e.getLocalizedMessage()))),
              HttpStatus.OK);
        }
    }

    @PostMapping("/product/update")
    @PreAuthorize("hasRole('ROLE_STORE')")
    public ResponseEntity<WrapperResponseEntity> updateProduct(@RequestBody ProductRequest productRequest) {
        log.info("Start updateProduct ProductRequest={}", productRequest);

        try {
            ProductEntity productEntity = storeService.updateProduct(productRequest);
            log.info("End updateProduct ProductRequest={}", productRequest);

            return new ResponseEntity<>(new WrapperResponseEntity(productEntity), HttpStatus.OK);
        } catch (Exception e) {
            log.error("#Error updateProduct, error={}", e.getLocalizedMessage());
            return new ResponseEntity<>(
              new WrapperResponseEntity(
                new ErrorResponse.ErrorObject(
                  ErrorConstants.FAILURE, Collections.singletonList(e.getLocalizedMessage()))),
              HttpStatus.OK);
        }
    }

    @PostMapping("/product/deactivate/{productId}")
    @PreAuthorize("hasRole('ROLE_STORE')")
    public ResponseEntity<WrapperResponseEntity> deactivateProduct(@PathVariable Long productId, @RequestParam boolean activate) {
        log.info("Start deactivateProduct productId={}", productId);

        try {
            Long deactivateProduct = storeService.deactivateProduct(productId, activate);
            log.info("End deactivateProduct productId={}", productId);

            return new ResponseEntity<>(new WrapperResponseEntity(deactivateProduct), HttpStatus.OK);
        } catch (Exception e) {
            log.error("#Error updateProduct, error={}", e.getLocalizedMessage());
            return new ResponseEntity<>(
                new WrapperResponseEntity(
                    new ErrorResponse.ErrorObject(
                        ErrorConstants.FAILURE, Collections.singletonList(e.getLocalizedMessage()))),
                HttpStatus.OK);
        }
    }

    @GetMapping("/products")
    @PreAuthorize("hasRole('ROLE_STORE')")
    public ResponseEntity<WrapperResponseEntity> getPagingProducts(
        @RequestParam(value = "limit", required = false, defaultValue = "20") Integer limit,
        @RequestParam(value = "offset", required = false, defaultValue = "0") Integer offset,
        @RequestParam(value = "orderBy", required = false) String orderBy) {
        log.info("Start getPagingProducts.");

        try {
            Collection<ProductEntity> productEntities = storeService.getPagingProducts(limit, offset, orderBy);
            log.info("End getPagingProducts.");

            Integer productCount = storeService.getStoreProductCount();
            PagingResponse pagingResponse = new PagingResponse(limit, offset, null, productCount);

            return new ResponseEntity<>(new WrapperResponseEntity(productEntities, pagingResponse), HttpStatus.OK);
        } catch (Exception e) {
            log.error("#Error getPagingProducts, error={}", e.getLocalizedMessage());
            return new ResponseEntity<>(
                new WrapperResponseEntity(
                    new ErrorResponse.ErrorObject(
                        ErrorConstants.FAILURE, Collections.singletonList(e.getLocalizedMessage()))),
                HttpStatus.OK);
        }
    }

    @GetMapping("/orders")
    @PreAuthorize("hasRole('ROLE_STORE')")
    public ResponseEntity<WrapperResponseEntity> getOrderStoreDetails(@RequestParam(required = false, defaultValue = "5") Integer limit,
        @RequestParam(required = false, defaultValue = "0") Integer offset) {
        log.info("Start getOrderStoreDetails");

        try {
            Collection<OrderStoreDetailEntity> orderStoreDetailEntities = storeService.getOrderStoreDetail(limit, offset);
            log.info("End getOrderStoreDetails");

            Integer productCount = storeService.getOrderStoreDetailCount();
            PagingResponse pagingResponse = new PagingResponse(limit, offset, null, productCount);

            return new ResponseEntity<>(new WrapperResponseEntity(orderStoreDetailEntities, pagingResponse), HttpStatus.OK);
        } catch (Exception e) {
            log.error("#Error getOrderStoreDetails, error={}", e.getLocalizedMessage());
            return new ResponseEntity<>(
                new WrapperResponseEntity(
                    new ErrorResponse.ErrorObject(
                        ErrorConstants.FAILURE, Collections.singletonList(e.getLocalizedMessage()))),
                HttpStatus.OK);
        }
    }

    @PostMapping("/order/{orderStoreDetailId}/accept")
    @PreAuthorize("hasRole('ROLE_STORE')")
    public ResponseEntity<WrapperResponseEntity> acceptOrderStoreDetail(@PathVariable Long orderStoreDetailId) {
        log.info("Start acceptOrderStoreDetail, orderStoreDetailId = {}", orderStoreDetailId);

        try {
            Long orderStoreDetail = storeService.updateOrderStoreDetailStatus(OrderStatus.ACCEPTED, orderStoreDetailId);
            log.info("End acceptOrderStoreDetail, orderStoreDetailId = {}", orderStoreDetailId);

            return new ResponseEntity<>(new WrapperResponseEntity(orderStoreDetail), HttpStatus.OK);
        } catch (Exception e) {
            log.error("#Error acceptOrderStoreDetail orderStoreDetailId={}, error={}", orderStoreDetailId,
                e.getLocalizedMessage());
            return new ResponseEntity<>(
                new WrapperResponseEntity(
                    new ErrorResponse.ErrorObject(
                        ErrorConstants.FAILURE, Collections.singletonList(e.getLocalizedMessage()))),
                HttpStatus.OK);
        }
    }

    @PostMapping("/order/{orderStoreDetailId}/decline")
    @PreAuthorize("hasRole('ROLE_STORE')")
    public ResponseEntity<WrapperResponseEntity> declineOrderStoreDetail(@PathVariable Long orderStoreDetailId) {
        log.info("Start declineOrderStoreDetail, orderStoreDetailId = {}", orderStoreDetailId);

        try {
            Long orderStoreDetail = storeService.updateOrderStoreDetailStatus(OrderStatus.CANCELLED,
                orderStoreDetailId);
            log.info("End declineOrderStoreDetail, orderStoreDetailId = {}", orderStoreDetailId);

            return new ResponseEntity<>(new WrapperResponseEntity(orderStoreDetail), HttpStatus.OK);
        } catch (Exception e) {
            log.error("#Error declineOrderStoreDetail orderStoreDetailId={}, error={}", orderStoreDetailId,
                e.getLocalizedMessage());
            return new ResponseEntity<>(
                new WrapperResponseEntity(
                    new ErrorResponse.ErrorObject(
                        ErrorConstants.FAILURE, Collections.singletonList(e.getLocalizedMessage()))),
                HttpStatus.OK);
        }
    }

    @PostMapping("/order/{orderStoreDetailId}/ship")
    @PreAuthorize("hasRole('ROLE_STORE')")
    public ResponseEntity<WrapperResponseEntity> shipOrderStoreDetail(@PathVariable Long orderStoreDetailId) {
        log.info("Start shipOrderStoreDetail, orderStoreDetailId = {}", orderStoreDetailId);

        try {
            Long orderStoreDetail = storeService.updateOrderStoreDetailStatus(OrderStatus.COMPLETED,
                orderStoreDetailId);
            log.info("End shipOrderStoreDetail, orderStoreDetailId = {}", orderStoreDetailId);

            return new ResponseEntity<>(new WrapperResponseEntity(orderStoreDetail), HttpStatus.OK);
        } catch (Exception e) {
            log.error("#Error shipOrderStoreDetail orderStoreDetailId={}, error={}", orderStoreDetailId,
                e.getLocalizedMessage());
            return new ResponseEntity<>(
                new WrapperResponseEntity(
                    new ErrorResponse.ErrorObject(
                        ErrorConstants.FAILURE, Collections.singletonList(e.getLocalizedMessage()))),
                HttpStatus.OK);
        }
    }
}
