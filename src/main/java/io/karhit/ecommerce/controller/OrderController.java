package io.karhit.ecommerce.controller;

import io.karhit.ecommerce.entity.OrderEntity;
import io.karhit.ecommerce.entity.OrderProductDetailEntity;
import io.karhit.ecommerce.entity.OrderStoreDetailEntity;
import io.karhit.ecommerce.entity.request.CheckoutRequest;
import io.karhit.ecommerce.entity.response.ErrorConstants;
import io.karhit.ecommerce.entity.response.ErrorResponse;
import io.karhit.ecommerce.entity.response.PagingResponse;
import io.karhit.ecommerce.entity.response.WrapperResponseEntity;
import io.karhit.ecommerce.service.OrderService;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/orders")
@Slf4j
public class OrderController {

    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping("/create")
    public ResponseEntity<WrapperResponseEntity> createOrder(@RequestBody CheckoutRequest checkoutRequest) {
        log.info("Start createOrder checkoutRequest={}", checkoutRequest);

        try {
            Long orderId = orderService.save(checkoutRequest);
            log.info("End createOrder checkoutRequest={}", checkoutRequest);

            return new ResponseEntity<>(new WrapperResponseEntity(Collections.singletonMap("orderId", orderId)),
                HttpStatus.OK);
        } catch (Exception e) {
            log.error("#Error createOrder, error={}", e.getLocalizedMessage());
            return new ResponseEntity<>(
                    new WrapperResponseEntity(
                            new ErrorResponse.ErrorObject(
                                    ErrorConstants.FAILURE, Collections.singletonList(e.getLocalizedMessage()))),
                    HttpStatus.OK);
        }
    }

    @GetMapping("/{orderId}")
    public ResponseEntity<WrapperResponseEntity> getOrder(@PathVariable Long orderId) {
        log.info("Start getOrder orderId={}", orderId);

        try {
            OrderEntity order = orderService.getOrderById(orderId);
            log.info("End getOrder orderId={}", orderId);

            return new ResponseEntity<>(new WrapperResponseEntity(order), HttpStatus.OK);
        } catch (Exception e) {
            log.error("#Error getOrder, error={}", e.getLocalizedMessage());
            return new ResponseEntity<>(
                    new WrapperResponseEntity(
                            new ErrorResponse.ErrorObject(
                                    ErrorConstants.FAILURE, Collections.singletonList(e.getLocalizedMessage()))),
                    HttpStatus.OK);
        }
    }

    @GetMapping("/store/{orderStoreId}")
    public ResponseEntity<WrapperResponseEntity> getOrderStoreDetail(@PathVariable Long orderStoreId) {
        log.info("Start getOrderStoreDetail orderStoreId={}", orderStoreId);

        try {
            OrderStoreDetailEntity orderStoreDetail = orderService.getOrderStoreDetailById(orderStoreId);
            log.info("End getOrderStoreDetail orderStoreId={}", orderStoreId);

            return new ResponseEntity<>(new WrapperResponseEntity(orderStoreDetail), HttpStatus.OK);
        } catch (Exception e) {
            log.error("#Error getOrderStoreDetail, error={}", e.getLocalizedMessage());
            return new ResponseEntity<>(
                new WrapperResponseEntity(
                    new ErrorResponse.ErrorObject(
                        ErrorConstants.FAILURE, Collections.singletonList(e.getLocalizedMessage()))),
                HttpStatus.OK);
        }
    }

    @GetMapping("/store/{orderStoreId}/products")
    public ResponseEntity<WrapperResponseEntity> getOrderProductDetails(@PathVariable Long orderStoreId) {
        log.info("Start getOrderStoreDetails orderStoreId={}", orderStoreId);

        try {
            List<OrderProductDetailEntity> orderStoreDetails = orderService.getOrderProductDetailsByOrderStoreDetailId(orderStoreId);
            log.info("End getOrderStoreDetails orderStoreId={}", orderStoreId);

            return new ResponseEntity<>(new WrapperResponseEntity(orderStoreDetails), HttpStatus.OK);
        } catch (Exception e) {
            log.error("#Error getOrderStoreDetails, error={}", e.getLocalizedMessage());
            return new ResponseEntity<>(
                new WrapperResponseEntity(
                    new ErrorResponse.ErrorObject(
                        ErrorConstants.FAILURE, Collections.singletonList(e.getLocalizedMessage()))),
                HttpStatus.OK);
        }
    }

    @GetMapping("/store/product/{orderProductId}")
    public ResponseEntity<WrapperResponseEntity> getOrderProductDetail(@PathVariable Long orderProductId) {
        log.info("Start getOrderStoreDetail orderProductId={}", orderProductId);

        try {
            OrderProductDetailEntity orderProductDetail = orderService.getOrderProductDetailById(orderProductId);
            log.info("End getOrderProductDetail orderProductId={}", orderProductId);

            return new ResponseEntity<>(new WrapperResponseEntity(orderProductDetail), HttpStatus.OK);
        } catch (Exception e) {
            log.error("#Error getOrderProductDetail, error={}", e.getLocalizedMessage());
            return new ResponseEntity<>(
                new WrapperResponseEntity(
                    new ErrorResponse.ErrorObject(
                        ErrorConstants.FAILURE, Collections.singletonList(e.getLocalizedMessage()))),
                HttpStatus.OK);
        }
    }

    @GetMapping("/user/orders")
    public ResponseEntity<WrapperResponseEntity> getOrdersFromUser(@RequestParam(required = false, defaultValue = "5") Integer limit,
        @RequestParam(required = false, defaultValue = "0") Integer offset) {
        log.info("Start getOrdersFromUser");

        try {
            Collection<OrderEntity> ordersResponse = orderService.getOrdersFromUser(limit, offset);
            log.info("End getOrdersFromUser");

            Integer productCount = orderService.getUserOrderCount();
            PagingResponse pagingResponse = new PagingResponse(limit, offset, null, productCount);

            return new ResponseEntity<>(new WrapperResponseEntity(ordersResponse, pagingResponse), HttpStatus.OK);
        } catch (Exception e) {
            log.error("#Error getOrdersFromUser, error={}", e.getLocalizedMessage());
            return new ResponseEntity<>(
                new WrapperResponseEntity(
                    new ErrorResponse.ErrorObject(
                        ErrorConstants.FAILURE, Collections.singletonList(e.getLocalizedMessage()))),
                HttpStatus.OK);
        }
    }

    @GetMapping("/store/orders")
    @PreAuthorize("hasRole('ROLE_STORE')")
    public ResponseEntity<WrapperResponseEntity> getOrdersFromStore(@RequestParam(required = false, defaultValue = "5") Integer limit,
        @RequestParam(required = false, defaultValue = "0") Integer offset) {
        log.info("Start getOrdersFromStore");

        try {
            Collection<OrderEntity> ordersResponse = orderService.getOrdersFromStore(limit, offset);
            log.info("End getOrdersFromStore");

            Integer productCount = orderService.getStoreOrderCount();
            PagingResponse pagingResponse = new PagingResponse(limit, offset, null, productCount);

            return new ResponseEntity<>(new WrapperResponseEntity(ordersResponse, pagingResponse), HttpStatus.OK);
        } catch (Exception e) {
            log.error("#Error getOrdersFromStore, error={}", e.getLocalizedMessage());
            return new ResponseEntity<>(
                new WrapperResponseEntity(
                    new ErrorResponse.ErrorObject(
                        ErrorConstants.FAILURE, Collections.singletonList(e.getLocalizedMessage()))),
                HttpStatus.OK);
        }
    }

    @GetMapping("/stores")
    public ResponseEntity<WrapperResponseEntity> getOrderStoreDetailsByOrderId(@RequestParam Long orderId) {
        log.info("Start getOrderStoreDetailsByOrderId, orderId = " + orderId);

        try {
            Collection<OrderStoreDetailEntity> orderStoreDetailEntities = orderService.getOrderStoreDetailsByOrderId(orderId);
            log.info("End getOrderStoreDetailsByOrderId, orderId = " + orderId);

            return new ResponseEntity<>(new WrapperResponseEntity(orderStoreDetailEntities), HttpStatus.OK);
        } catch (Exception e) {
            log.error("#Error getOrdersFromUser, error={}", e.getLocalizedMessage());
            return new ResponseEntity<>(
                new WrapperResponseEntity(
                    new ErrorResponse.ErrorObject(
                        ErrorConstants.FAILURE, Collections.singletonList(e.getLocalizedMessage()))),
                HttpStatus.OK);
        }
    }
}
