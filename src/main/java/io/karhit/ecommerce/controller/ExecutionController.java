package io.karhit.ecommerce.controller;

import io.karhit.ecommerce.entity.UserEntity;
import io.karhit.ecommerce.entity.response.ErrorConstants;
import io.karhit.ecommerce.entity.response.ErrorResponse.ErrorObject;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.karhit.ecommerce.entity.request.ExecuteRequest;
import io.karhit.ecommerce.entity.response.WrapperResponseEntity;
import io.karhit.ecommerce.service.ExecuteService;

@RestController
@RequestMapping("/api/v1/execute")
@CrossOrigin
@Slf4j
public class ExecutionController {

  private final ExecuteService executeService;

  public ExecutionController(ExecuteService executeService) {
    this.executeService = executeService;
  }

  @PostMapping("")
  public ResponseEntity<WrapperResponseEntity> execute(@RequestBody ExecuteRequest request) {
    log.info("Start execution id={}", request.getId());

    try {
      List<UserEntity> users = executeService.execute(request);

      log.info("End execution id={}", request.getId());
      return new ResponseEntity<>(new WrapperResponseEntity(users), HttpStatus.OK);

    } catch (Exception e) {
      return new ResponseEntity<>(
          new WrapperResponseEntity(new ErrorObject(ErrorConstants.FAILURE, null)),
          HttpStatus.OK);
    }

  }
}
