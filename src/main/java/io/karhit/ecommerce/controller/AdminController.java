package io.karhit.ecommerce.controller;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/public")
public class AdminController {

  @Value("${git.commit.message.short}")
  private String commitMessage;

  @Value("${git.branch}")
  private String branch;

  @Value("${git.commit.id}")
  private String commitId;

  @GetMapping("/commit")
  public Map<String, String> getCommitId() {
    Map<String, String> result = new HashMap<>();
    result.put("Commit message", commitMessage);
    result.put("Commit branch", branch);
    result.put("Commit id", commitId);
    return result;
  }

}
