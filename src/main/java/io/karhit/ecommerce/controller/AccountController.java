package io.karhit.ecommerce.controller;

import io.karhit.ecommerce.entity.UserAddressEntity;
import io.karhit.ecommerce.entity.UserEntity;
import io.karhit.ecommerce.entity.request.UserAddressRequest;
import io.karhit.ecommerce.entity.response.ErrorConstants;
import io.karhit.ecommerce.entity.response.ErrorResponse;
import io.karhit.ecommerce.entity.response.WrapperResponseEntity;
import io.karhit.ecommerce.exception.ResourcesNotFoundException;
import io.karhit.ecommerce.service.UserService;
import java.util.Collections;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api")
public class AccountController {

    private final UserService userService;

    public AccountController(UserService userService) {
        this.userService = userService;
    }

    /**
     * {@code GET  /account} : get the current user.
     *
     * @return the current user.
     * @throws RuntimeException {@code 500 (Internal Server Error)} if the user couldn't be returned.
     */
    @GetMapping("/account")
    public ResponseEntity<WrapperResponseEntity> getAccount() {
        log.info("AccountController.getAccount() Start get user info....");

        Optional<UserEntity> optUserInfo = userService.getUserWithAuthorities();

        if (!optUserInfo.isPresent()) {
            throw new ResourcesNotFoundException("User could not be found");
        }

        log.info("AccountController.getAccount() End get user info....");
        return new ResponseEntity<>(new WrapperResponseEntity(optUserInfo.get()), HttpStatus.OK);
    }

    @GetMapping("/account/address")
    public ResponseEntity<WrapperResponseEntity> getUserAddress() {
        log.info("Start get user address....");

        try {
            Optional<UserAddressEntity> userAddress = userService.getUserAddress();
            log.info("End get user address....");

            if (userAddress.isPresent()) {
                return new ResponseEntity<>(new WrapperResponseEntity(userAddress.get()), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(
                    new WrapperResponseEntity(
                        new ErrorResponse.ErrorObject(
                            ErrorConstants.FAILURE, Collections.singletonList("User haven't set any address yet."))),
                    HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("#Error get user address, error={}", e.getLocalizedMessage());
            return new ResponseEntity<>(
                new WrapperResponseEntity(
                    new ErrorResponse.ErrorObject(
                        ErrorConstants.FAILURE, Collections.singletonList(e.getLocalizedMessage()))),
                HttpStatus.OK);
        }
    }

    @PostMapping("/account/address")
    public ResponseEntity<WrapperResponseEntity> setUserAddress(@RequestBody UserAddressRequest userAddressRequest) {
        log.info("Start save user address....");

        try {
            UserAddressEntity userAddress = userService.saveUserAddress(userAddressRequest);
            log.info("End save user address....");

            return new ResponseEntity<>(new WrapperResponseEntity(userAddress), HttpStatus.OK);
        } catch (Exception e) {
            log.error("#Error save user address, error={}", e.getLocalizedMessage());
            return new ResponseEntity<>(
                new WrapperResponseEntity(
                    new ErrorResponse.ErrorObject(
                        ErrorConstants.FAILURE, Collections.singletonList(e.getLocalizedMessage()))),
                HttpStatus.OK);
        }
    }

}

