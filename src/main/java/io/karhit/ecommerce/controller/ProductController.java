package io.karhit.ecommerce.controller;

import io.karhit.ecommerce.entity.ProductEntity;
import io.karhit.ecommerce.entity.request.ProductRequest;
import io.karhit.ecommerce.entity.response.ErrorConstants;
import io.karhit.ecommerce.entity.response.ErrorResponse;
import io.karhit.ecommerce.entity.response.PagingResponse;
import io.karhit.ecommerce.entity.response.WrapperResponseEntity;
import io.karhit.ecommerce.service.ProductService;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/products")
@Slf4j
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/paging")
    public ResponseEntity<WrapperResponseEntity> getProductPaging(
        @RequestParam(value = "limit", required = false, defaultValue = "20") Integer limit,
        @RequestParam(value = "offset", required = false, defaultValue = "0") Integer offset,
        @RequestParam(value = "orderBy", required = false) String orderBy) {
        log.info("Start getProductPaging with limit={}, offset = {}, orderBy = {}", limit, offset, orderBy);

        try {
            Collection<ProductEntity> products = productService.getPagingProducts(limit, offset, orderBy);
            Integer productCount = productService.getCount();
            PagingResponse pagingResponse = new PagingResponse(limit, offset, orderBy, productCount);

            log.info("End getProductPaging with limit={}, offset = {}, orderBy = {}", limit, offset, orderBy);
            return new ResponseEntity<>(new WrapperResponseEntity(products, pagingResponse), HttpStatus.OK);
        } catch (Exception e) {
            log.error("#Error getProductPaging with limit={}, offset = {}, orderBy = {}, error = {}", limit, offset,
                orderBy, e.getMessage());
            return new ResponseEntity<>(
                new WrapperResponseEntity(
                    new ErrorResponse.ErrorObject(
                        ErrorConstants.FAILURE, Collections.singletonList(e.getLocalizedMessage()))),
                HttpStatus.OK);
        }
    }

    @GetMapping("")
    public ResponseEntity<WrapperResponseEntity> getProducts(@RequestParam Long... pIds) {
        Set<Long> ids = new HashSet<>(Arrays.asList(pIds));
        log.info("Start getProducts pIds={}", ids);

        try {
            Collection<ProductEntity> products = productService.getAll(ids);

            log.info("End getProducts pIds={}", ids);
            return new ResponseEntity<>(new WrapperResponseEntity(products), HttpStatus.OK);

        } catch (Exception e) {
            log.error("#Error getProducts, error={}", e.getLocalizedMessage());
            return new ResponseEntity<>(
                    new WrapperResponseEntity(
                            new ErrorResponse.ErrorObject(
                                    ErrorConstants.FAILURE, Collections.singletonList(e.getLocalizedMessage()))),
                    HttpStatus.OK);
        }
    }

    @GetMapping("/{pId}")
    public ResponseEntity<WrapperResponseEntity> getProduct(@PathVariable Long pId) {
        log.info("Start getProduct pId={}", pId);

        try {
            ProductEntity product = productService.getById(pId);
            log.info("End getProducts pIds={}", pId);

            return new ResponseEntity<>(new WrapperResponseEntity(product), HttpStatus.OK);
        } catch (Exception e) {
            log.error("#Error getProduct, error={}", e.getLocalizedMessage());
            return new ResponseEntity<>(
                new WrapperResponseEntity(
                    new ErrorResponse.ErrorObject(
                        ErrorConstants.FAILURE, Collections.singletonList(e.getLocalizedMessage()))),
                HttpStatus.OK);
        }
    }

    @GetMapping("/store/{productId}")
    public ResponseEntity<WrapperResponseEntity> getStoreId(@PathVariable Long productId) {
        log.info("Start getStoreId by productId={}", productId);

        try {
            Long storeId = productService.getStoreId(productId);
            log.info("End getStoreId by productId={}", productId);

            return new ResponseEntity<>(new WrapperResponseEntity(Collections.singletonMap("storeId", storeId)),
                HttpStatus.OK);
        } catch (Exception e) {
            log.error("#Error getProduct, error={}", e.getLocalizedMessage());
            return new ResponseEntity<>(
                new WrapperResponseEntity(
                    new ErrorResponse.ErrorObject(
                        ErrorConstants.FAILURE, Collections.singletonList(e.getLocalizedMessage()))),
                HttpStatus.OK);
        }
    }

    @GetMapping("/store/{storeId}/products")
    public ResponseEntity<WrapperResponseEntity> getProductsFromStore(@PathVariable Long storeId,
        @RequestParam(required = false, defaultValue = "20") Integer limit,
        @RequestParam(required = false, defaultValue = "0") Integer offset) {
        log.info("Start getProductsFromStore by storeId={}", storeId);

        try {
            Collection<ProductEntity> productEntities = productService.getStoreProducts(storeId, limit, offset);
            log.info("End getProductsFromStore by storeId={}", storeId);

            Integer productCount = productService.getStoreProductCount(storeId);
            PagingResponse pagingResponse = new PagingResponse(limit, offset, null, productCount);

            return new ResponseEntity<>(new WrapperResponseEntity(productEntities, pagingResponse),
                HttpStatus.OK);
        } catch (Exception e) {
            log.error("#Error getProductsFromStore, error={}", e.getLocalizedMessage());
            return new ResponseEntity<>(
                new WrapperResponseEntity(
                    new ErrorResponse.ErrorObject(
                        ErrorConstants.FAILURE, Collections.singletonList(e.getLocalizedMessage()))),
                HttpStatus.OK);
        }
    }

    @PostMapping("/create")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<WrapperResponseEntity> createProduct(@RequestBody ProductRequest productRequest) {
        log.info("Start createProduct createProductRequest={}", productRequest);

        try {
            ProductEntity product = productService.save(productRequest);
            log.info("End createProduct createProductRequest={}", productRequest);

            return new ResponseEntity<>(new WrapperResponseEntity(product), HttpStatus.OK);
        } catch (Exception e) {
            log.error("#Error createProduct, error={}", e.getLocalizedMessage());
            return new ResponseEntity<>(
              new WrapperResponseEntity(
                            new ErrorResponse.ErrorObject(
                                    ErrorConstants.FAILURE, Collections.singletonList(e.getLocalizedMessage()))),
                    HttpStatus.OK);
        }
    }
}
