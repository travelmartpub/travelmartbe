package io.karhit.ecommerce.controller;

import io.karhit.ecommerce.entity.response.WrapperResponseEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("")
@Slf4j
public class IndexController {

  @GetMapping("/ping")
  public ResponseEntity<String> ping() {
    log.info("pong");
    return ResponseEntity.ok("pong");
  }

  @GetMapping("/")
  public ResponseEntity<String> home() {
    log.info("Hello");
    return new ResponseEntity<>("Hello", HttpStatus.OK);
  }
}
