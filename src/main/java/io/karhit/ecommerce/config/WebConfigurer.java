package io.karhit.ecommerce.config;

import java.util.Arrays;
import java.util.Collections;
import javax.servlet.ServletContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.server.WebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * Configuration of web application with Servlet 3.0 APIs.
 */
@Configuration
@Slf4j
public class WebConfigurer implements ServletContextInitializer,
    WebServerFactoryCustomizer<WebServerFactory> {

  private final Environment env;

  public WebConfigurer(Environment env) {
    this.env = env;
  }

  @Override
  public void onStartup(ServletContext servletContext) {
    if (env.getActiveProfiles().length != 0) {
      log.info("Web application configuration, using profiles: {}",
          (Object[]) env.getActiveProfiles());
    }

    log.info("Web application fully configured");
  }

  @Bean
  public CorsFilter corsFilter() {
    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    CorsConfiguration config = getCors();
    if (config.getAllowedOrigins() != null && !config.getAllowedOrigins().isEmpty()) {
      log.debug("Registering CORS filter");
      source.registerCorsConfiguration("/api/**", config);
      source.registerCorsConfiguration("/auth/**", config);
      source.registerCorsConfiguration("/management/**", config);
    }
    return new CorsFilter(source);
  }

  /**
   * <p>Getter for the field <code>cors</code>.</p>
   *
   * @return a {@link org.springframework.web.cors.CorsConfiguration} object.
   */
  private CorsConfiguration getCors() {
    CorsConfiguration corsN = new CorsConfiguration();
    corsN.setAllowedOrigins(Collections.singletonList("*"));
    corsN.setAllowedMethods(Collections.singletonList("*"));
    corsN.setAllowedHeaders(Collections.singletonList("*"));
    corsN.setExposedHeaders(Arrays.asList("Authorization", "Link", "X-Total-Count"));
    return corsN;
  }

  @Override
  public void customize(WebServerFactory factory) {
  }
}
