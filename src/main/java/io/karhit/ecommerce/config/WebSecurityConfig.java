package io.karhit.ecommerce.config;

import io.karhit.ecommerce.security.AuthoritiesConstants;
import io.karhit.ecommerce.security.DomainUserDetailsService;
import io.karhit.ecommerce.security.jwt.JWTConfigurer;
import io.karhit.ecommerce.security.jwt.TokenProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.header.writers.ReferrerPolicyHeaderWriter;
import org.springframework.web.filter.CorsFilter;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  private final TokenProvider tokenProvider;
  private final CorsFilter corsFilter;
  private final DomainUserDetailsService domainUserDetailsService;
  private final AuthenticationEntryPoint authenticationEntryPoint;

  public WebSecurityConfig(TokenProvider tokenProvider,
      CorsFilter corsFilter,
      DomainUserDetailsService domainUserDetailsService,
      AuthenticationEntryPoint authenticationEntryPoint) {
    this.tokenProvider = tokenProvider;
    this.corsFilter = corsFilter;
    this.domainUserDetailsService = domainUserDetailsService;
    this.authenticationEntryPoint = authenticationEntryPoint;
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Bean
  public DaoAuthenticationProvider daoAuthenticationProvider() {
    DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
    provider.setPasswordEncoder(passwordEncoder());
    provider.setUserDetailsService(this.domainUserDetailsService);
    return provider;
  }

  @Override
  public void configure(WebSecurity web) {
    web.ignoring()
        .antMatchers(HttpMethod.OPTIONS, "/**")
        .antMatchers("/app/**/*.{js,html}")
        .antMatchers("/i18n/**")
        .antMatchers("/content/**")
        .antMatchers("/swagger-ui/index.html")
        .antMatchers("/test/**");
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {

    http
        .csrf()
        .disable()
        .addFilterBefore(corsFilter, UsernamePasswordAuthenticationFilter.class)
        .exceptionHandling()
        .authenticationEntryPoint(authenticationEntryPoint)
        .and()
        .headers()
        .contentSecurityPolicy(
            "default-src 'self'; frame-src 'self' data:; script-src 'self' 'unsafe-inline' 'unsafe-eval' https://storage.googleapis.com; style-src 'self' 'unsafe-inline'; img-src 'self' data:; font-src 'self' data:")
        .and()
        .referrerPolicy(ReferrerPolicyHeaderWriter.ReferrerPolicy.STRICT_ORIGIN_WHEN_CROSS_ORIGIN)
        .and()
        .featurePolicy(
            "geolocation 'none'; midi 'none'; sync-xhr 'none'; microphone 'none'; camera 'none'; magnetometer 'none'; gyroscope 'none'; speaker 'none'; fullscreen 'self'; payment 'none'")
        .and()
        .frameOptions()
        .deny()
        .and()
        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .authorizeRequests()
        .antMatchers("/auth/**").permitAll()
        .antMatchers("/public/**").permitAll()
        .antMatchers("/api/v1/products").permitAll()
        .antMatchers("/api/v1/products/{**}").permitAll()
        .antMatchers("/api/v1/products/paging").permitAll()
        .antMatchers("/api/v1/products/store/{**}").permitAll()
        .antMatchers("/api/v1/products/store/{**}/products").permitAll()
        .antMatchers("/api/v1/stores/{**}").permitAll()
        .antMatchers("/management/**").hasAuthority(AuthoritiesConstants.ADMIN)
        .antMatchers("/admin/**").hasAuthority(AuthoritiesConstants.ADMIN)
        .antMatchers("/actuator/**", "/health", "/info", "/metrics", "/ping").permitAll()
        .antMatchers("/api/**").authenticated()
        .and()
        .httpBasic()
        .and()
        .apply(new JWTConfigurer(tokenProvider));
  }
}
