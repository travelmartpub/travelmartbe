package io.karhit.ecommerce.config;

import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.AbstractRequestLoggingFilter;

@Configuration
public class LoggingFilterConfig extends AbstractRequestLoggingFilter {

  private static final Logger log = LoggerFactory.getLogger(LoggingFilterConfig.class);

  @Override
  protected void beforeRequest(HttpServletRequest request, String message) {
    if (message.contains("/health") || message.contains("/info") || message.contains("/metrics"))
      return;

    log.info(message);
  }

  @Override
  protected void afterRequest(HttpServletRequest request, String message) {
    if (message.contains("/health") || message.contains("/info") || message.contains("/metrics"))
      return;

    log.info(message);
  }
}
