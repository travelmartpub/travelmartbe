package io.karhit.ecommerce.config;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.embedded.jetty.JettyServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;

@Configurable
public class JettyConfig {

  @Value("${server.port:8080}")
  private int port;

  @Bean
  public ConfigurableServletWebServerFactory webServerFactory() {
    JettyServletWebServerFactory factory = new JettyServletWebServerFactory();
    factory.setPort(port);

    Map<String, String> initParams = new HashMap<>();
    initParams.put("dirAllowed", "false");
    initParams.put("welcomeServlets", "true");
    initParams.put("redirectWelcome", "true");
    factory.setInitParameters(initParams);

    return factory;
  }
}
