package io.karhit.ecommerce.config.object;

import javax.sql.DataSource;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

@Getter
@Setter
@Configuration
public class DataSourceConfig {

  @Bean
  @Primary
  public PlatformTransactionManager platformTransactionManager(DataSource dataSource) {
    return new DataSourceTransactionManager(dataSource);
  }

  @Bean
  @ConfigurationProperties(prefix = "app.datasource.ecommerce")
  @Primary
  public DataSource ecommerceDataSource() {
    return DataSourceBuilder.create().build();
  }

}
