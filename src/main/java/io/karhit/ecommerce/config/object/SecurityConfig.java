package io.karhit.ecommerce.config.object;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Setter
@Getter
@Configuration
@ConfigurationProperties(prefix = "app.security")
public class SecurityConfig {

  private String secret;
  private String base64Secret;
  private long tokenValidityInSeconds;
  private long tokenValidityInSecondsForRememberMe;
}
