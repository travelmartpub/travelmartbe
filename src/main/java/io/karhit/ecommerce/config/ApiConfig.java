package io.karhit.ecommerce.config;

import com.google.protobuf.util.JsonFormat;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.protobuf.ProtobufJsonFormatHttpMessageConverter;

@Configuration
public class ApiConfig {

  @Bean
  public ProtobufJsonFormatHttpMessageConverter protobufHttpMessageConverter() {
    JsonFormat.Printer printer = JsonFormat.printer().includingDefaultValueFields();
    return new ProtobufJsonFormatHttpMessageConverter(JsonFormat.parser(), printer);
  }
}
