package io.karhit.ecommerce.security;

import io.karhit.ecommerce.entity.UserAuthorityEntity;
import io.karhit.ecommerce.entity.UserEntity;
import io.karhit.ecommerce.exception.UserNotActivatedException;
import io.karhit.ecommerce.repository.UserAuthorityRepository;
import io.karhit.ecommerce.repository.UserRepository;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * Authenticate a user from the database.
 */
@Component("userDetailsService")
@Slf4j
public class DomainUserDetailsService implements UserDetailsService {

  private final UserRepository userRepository;
  private final UserAuthorityRepository userAuthRepository;

  public DomainUserDetailsService(UserRepository userRepository,
      UserAuthorityRepository userAuthRepository) {
    this.userRepository = userRepository;
    this.userAuthRepository = userAuthRepository;
  }

  @Override
  public UserDetails loadUserByUsername(final String login) {
    log.info("Authenticating {}", login);
    String lowercaseLogin = login.toLowerCase(Locale.ENGLISH);

    // Login via user name
    return userRepository.getUserByUserName(lowercaseLogin)
        .map(user -> createSpringSecurityUser(lowercaseLogin, user))
        .orElseThrow(() -> new UsernameNotFoundException(
            "User " + lowercaseLogin + " was not found in the database"));
  }

  private User createSpringSecurityUser(String lowercaseLogin, UserEntity user) {
    if (!user.isActivated()) {
      throw new UserNotActivatedException("User " + lowercaseLogin + " was not activated");
    }

    List<UserAuthorityEntity> authorityEntity = userAuthRepository
        .getAuthoritiesByUserId(user.getId());

    log.info("User {} role {}", user.getUserName(), authorityEntity);

    List<GrantedAuthority> grantedAuthorities = authorityEntity.stream()
        .map(authority -> new SimpleGrantedAuthority(authority.getName()))
        .collect(Collectors.toList());

    return new User(user.getUserName(), user.getPassword(), grantedAuthorities);
  }
}
