package io.karhit.ecommerce.service;

import io.karhit.ecommerce.entity.OrderEntity;
import io.karhit.ecommerce.entity.OrderProductDetailEntity;
import io.karhit.ecommerce.entity.OrderStoreDetailEntity;
import io.karhit.ecommerce.entity.request.CheckoutRequest;
import io.karhit.ecommerce.exception.DatabaseException;
import java.util.Collection;
import java.util.List;

public interface OrderService {

  OrderEntity getOrderById(Long orderId);

  OrderStoreDetailEntity getOrderStoreDetailById(Long orderStoreDetailId);

  OrderProductDetailEntity getOrderProductDetailById(Long orderProductDetailId);

  List<OrderProductDetailEntity> getOrderProductDetailsByOrderStoreDetailId(Long orderStoreDetailId);

  Collection<OrderStoreDetailEntity> getOrderStoreDetailsByOrderId(Long orderId);

  Collection<OrderEntity> getOrdersFromUser(Integer limit, Integer offset);

  Collection<OrderEntity> getOrdersFromStore(Integer limit, Integer offset);

  Long save(CheckoutRequest checkoutRequest) throws DatabaseException;

  Integer getUserOrderCount() throws DatabaseException;

  Integer getStoreOrderCount() throws DatabaseException;
}
