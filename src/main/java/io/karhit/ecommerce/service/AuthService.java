package io.karhit.ecommerce.service;

import io.karhit.ecommerce.entity.UserEntity;
import io.karhit.ecommerce.exception.DatabaseException;

public interface AuthService {

  UserEntity register(String userName, String email, String password) throws DatabaseException;

}
