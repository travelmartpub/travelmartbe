package io.karhit.ecommerce.service;

import io.karhit.ecommerce.entity.UserAddressEntity;
import io.karhit.ecommerce.entity.UserEntity;
import io.karhit.ecommerce.entity.request.UserAddressRequest;
import io.karhit.ecommerce.exception.DatabaseException;
import java.util.Optional;

public interface UserService {

    Optional<UserEntity> getUserWithAuthorities();

    Optional<UserAddressEntity> getUserAddress();

    UserAddressEntity saveUserAddress(UserAddressRequest userAddressEntity) throws DatabaseException;
}
