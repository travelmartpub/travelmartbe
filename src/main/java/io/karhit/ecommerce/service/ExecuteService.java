package io.karhit.ecommerce.service;

import io.karhit.ecommerce.entity.UserEntity;
import io.karhit.ecommerce.entity.request.ExecuteRequest;
import java.util.List;

public interface ExecuteService {

  List<UserEntity> execute(ExecuteRequest request);

}
