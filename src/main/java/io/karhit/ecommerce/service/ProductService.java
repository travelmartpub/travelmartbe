package io.karhit.ecommerce.service;

import io.karhit.ecommerce.entity.ProductEntity;
import io.karhit.ecommerce.entity.request.ProductRequest;
import io.karhit.ecommerce.exception.DatabaseException;
import java.util.Collection;
import java.util.Set;

public interface ProductService {

  Integer getCount() throws DatabaseException;

  Integer getStoreProductCount(Long storeId) throws DatabaseException;

  Collection<ProductEntity> getPagingProducts(Integer limit, Integer offset, String orderBy);

  Collection<ProductEntity> getStoreProducts(Long storeId, Integer limit, Integer offset);

  Collection<ProductEntity> getAll(Set<Long> pIds);

  ProductEntity getById(Long pId);

  Long getStoreId(Long productId);

  ProductEntity save(ProductRequest productRequest) throws DatabaseException;
}
