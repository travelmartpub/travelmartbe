package io.karhit.ecommerce.service.impl;

import io.karhit.ecommerce.entity.UserEntity;
import io.karhit.ecommerce.entity.request.ExecuteRequest;
import io.karhit.ecommerce.repository.UserRepository;
import io.karhit.ecommerce.service.ExecuteService;
import java.util.Collections;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ExecuteServiceImpl implements ExecuteService {

  private final UserRepository userRepository;

  public ExecuteServiceImpl(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public List<UserEntity> execute(ExecuteRequest request) {
    List<UserEntity> users;
    try {
      users = userRepository.getUsers(null);

      log.info("Execute with users={}", users);
    } catch (Exception e) {
      log.error("Execute was failure id={}, error={}", request.getId(), e.getMessage());
      return Collections.emptyList();
    }

    return users;
  }

}
