package io.karhit.ecommerce.service.impl;

import io.karhit.ecommerce.constants.Authority;
import io.karhit.ecommerce.entity.UserAuthorityEntity;
import io.karhit.ecommerce.entity.UserEntity;
import io.karhit.ecommerce.exception.DatabaseException;
import io.karhit.ecommerce.repository.UserAuthorityRepository;
import io.karhit.ecommerce.repository.UserRepository;
import io.karhit.ecommerce.service.AuthService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
public class AuthServiceImpl implements AuthService {

  private final PasswordEncoder passwordEncoder;
  private final UserRepository userRepository;
  private final UserAuthorityRepository userAuthorityRepository;

  public AuthServiceImpl(PasswordEncoder passwordEncoder, UserRepository userRepository,
      UserAuthorityRepository userAuthorityRepository) {
    this.passwordEncoder = passwordEncoder;
    this.userRepository = userRepository;
    this.userAuthorityRepository = userAuthorityRepository;
  }

  @Transactional
  @Override
  public UserEntity register(String username, String email, String password) throws DatabaseException {
    long userId = System.currentTimeMillis();

    // save info
    UserEntity userEntity = UserEntity.builder()
        .id(userId)
        .email(email)
        .userName(username)
        .activated(true)
        .password(passwordEncoder.encode(password))
        .storeId(0L)
        .build();
    userEntity = userRepository.save(userEntity);
    userEntity.setPassword("***");

    // save authority
    UserAuthorityEntity userAuthorityEntity = UserAuthorityEntity.builder()
        .userId(userEntity.getId())
        .name(Authority.ROLE_USER.getValue())
        .build();
    userAuthorityRepository.save(userAuthorityEntity);

    return userEntity;
  }
}
