package io.karhit.ecommerce.service.impl;

import io.karhit.ecommerce.constants.Authority;
import io.karhit.ecommerce.entity.Enum.OrderStatus;
import io.karhit.ecommerce.entity.OrderProductDetailEntity;
import io.karhit.ecommerce.entity.OrderStoreDetailEntity;
import io.karhit.ecommerce.entity.ProductEntity;
import io.karhit.ecommerce.entity.StoreEntity;
import io.karhit.ecommerce.entity.UserAuthorityEntity;
import io.karhit.ecommerce.entity.UserEntity;
import io.karhit.ecommerce.entity.request.CreateStoreRequest;
import io.karhit.ecommerce.entity.request.ProductRequest;
import io.karhit.ecommerce.exception.DatabaseException;
import io.karhit.ecommerce.exception.ResourcesNotFoundException;
import io.karhit.ecommerce.repository.OrderProductDetailRepository;
import io.karhit.ecommerce.repository.OrderStoreDetailRepository;
import io.karhit.ecommerce.repository.ProductRepository;
import io.karhit.ecommerce.repository.StoreRepository;
import io.karhit.ecommerce.repository.UserAuthorityRepository;
import io.karhit.ecommerce.repository.UserRepository;
import io.karhit.ecommerce.service.StoreService;
import io.karhit.ecommerce.util.SecurityUtils;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
public class StoreServiceImpl implements StoreService {

  private final StoreRepository storeRepository;
  private final UserRepository userRepository;
  private final ProductRepository productRepository;
  private final OrderStoreDetailRepository orderStoreDetailRepository;
  private final OrderProductDetailRepository orderProductDetailRepository;
  private final UserAuthorityRepository userAuthorityRepository;

  public StoreServiceImpl(StoreRepository storeRepository,
      UserRepository userRepository, ProductRepository productRepository,
      OrderStoreDetailRepository orderStoreDetailRepository,
      OrderProductDetailRepository orderProductDetailRepository,
      UserAuthorityRepository userAuthorityRepository) {
    this.storeRepository = storeRepository;
    this.userRepository = userRepository;
    this.productRepository = productRepository;
    this.orderStoreDetailRepository = orderStoreDetailRepository;
    this.orderProductDetailRepository = orderProductDetailRepository;
    this.userAuthorityRepository = userAuthorityRepository;
  }

  @Override
  public StoreEntity getById(Long storeId) {
    Optional<StoreEntity> store = storeRepository.getStoreById(storeId);

    if (!store.isPresent()) {
      throw new ResourcesNotFoundException(
          String.format("Store with id = %s not found", storeId));
    }

    return store.get();
  }

  @Override
  public StoreEntity getActivatedById(Long storeId) {
    Optional<StoreEntity> store = storeRepository.getStoreByIdAndActive(storeId, true);

    if (!store.isPresent()) {
      throw new ResourcesNotFoundException(
          String.format("Store with id = %s not found", storeId));
    }

    return store.get();
  }

  @Override
  public StoreEntity save(CreateStoreRequest createStoreRequest) throws DatabaseException {
    long productId = System.currentTimeMillis();

    StoreEntity storeEntity = StoreEntity.builder()
        .id(productId)
        .name(createStoreRequest.getName())
        .isActive(true)
        .build();

    storeEntity = storeRepository.save(storeEntity);

    return storeEntity;
  }

  @Transactional
  @Override
  public StoreEntity createUserStore(CreateStoreRequest createStoreRequest) throws DatabaseException {
    long storeId = System.currentTimeMillis();
    StoreEntity storeEntity = null;
    Optional<String> username = SecurityUtils.getCurrentUserLogin();

    if (username.isPresent()) {
      storeEntity = StoreEntity.builder()
          .id(storeId)
          .name(createStoreRequest.getName())
          .isActive(true)
          .build();

      Optional<UserEntity> user = userRepository.getUserByUserName(username.get());
      storeEntity = storeRepository.save(storeEntity);
      if (user.isPresent()) {
        userAuthorityRepository.save(new UserAuthorityEntity(user.get().getId(), Authority.ROLE_STORE.name()));
      }
      userRepository.setStoreId(username.get(), storeEntity.getId());
    }

    return storeEntity;
  }

  @Transactional
  @Override
  public Collection<ProductEntity> getPagingProducts(Integer limit, Integer offset, String orderBy) {
    Collection<ProductEntity> productEntities = null;
    try {
      Optional<String> username = SecurityUtils.getCurrentUserLogin();

      if (username.isPresent()) {
        Optional<Long> storeId = userRepository.getStoreId(username.get());

        if (storeId.isPresent()) {
          productEntities = productRepository.getStorePagingProducts(storeId.get(), limit, offset, orderBy);
        }
      }
    } catch (Exception e) {
      log.error("Get PagingProducts was failure, error={}", e.getMessage());
      throw e;
    }
    return productEntities;
  }

  @Transactional
  @Override
  public ProductEntity createProduct(ProductRequest productRequest) throws DatabaseException {
    try {
      Optional<String> username = SecurityUtils.getCurrentUserLogin();

      if (username.isPresent()) {
        Optional<Long> storeId = userRepository.getStoreId(username.get());

        if (storeId.isPresent()) {
          long productId = System.currentTimeMillis();
          ProductEntity productEntity = ProductEntity.builder()
              .id(productId)
              .name(productRequest.getName())
              .storeId(storeId.get())
              .description(productRequest.getDescription())
              .price(productRequest.getPrice())
              .stock(productRequest.getStock())
              .sold(0)
              .createdAt(new Date())
              .images(productRequest.getImages())
              .activated(true)
              .build();

          return productRepository.save(productEntity);
        }
      }
    } catch (Exception e) {
      log.error(e.getMessage());
      throw e;
    }
    return null;
  }

  @Transactional
  @Override
  public ProductEntity updateProduct(ProductRequest productRequest) throws DatabaseException {
    Long productId = productRepository.update(productRequest);

    Optional<ProductEntity> productEntity = productRepository.getProductById(productId);

    if (!productEntity.isPresent()) {
      throw new DatabaseException("Cannot update product = " + productRequest);
    }

    return productEntity.get();
  }

  @Override
  public Long deactivateProduct(Long productId, boolean activated) throws DatabaseException {
    return productRepository.deactivate(productId, activated);
  }

  @Transactional
  @Override
  public Collection<OrderStoreDetailEntity> getOrderStoreDetail(Integer limit, Integer offset) {
    Collection<OrderStoreDetailEntity> orderStoreDetailEntities = null;
    try {
      Optional<String> username = SecurityUtils.getCurrentUserLogin();

      if (username.isPresent()) {
        Optional<Long> storeId = userRepository.getStoreId(username.get());

        if (storeId.isPresent()) {
          orderStoreDetailEntities = orderStoreDetailRepository.getOrderStoreDetailsByStoreId(storeId.get(), limit, offset);

          for (OrderStoreDetailEntity orderStoreDetail : orderStoreDetailEntities) {
            orderStoreDetail.setOrderProductDetails(orderProductDetailRepository.getAllByOrderStoreDetailId(orderStoreDetail.getId()));
          }
        }
      }
    } catch (Exception e) {
      log.error("Get OrderStoreDetail was failure, error={}", e.getMessage());
      throw e;
    }
    return orderStoreDetailEntities;
  }

  @Transactional
  @Override
  public Long updateOrderStoreDetailStatus(OrderStatus status, Long orderStoreDetailId) throws DatabaseException {

    Collection<OrderProductDetailEntity> orderProductDetailEntities =
        orderProductDetailRepository.getAllByOrderStoreDetailId(orderStoreDetailId);

    for (OrderProductDetailEntity orderProductDetail : orderProductDetailEntities) {
      productRepository.addStock(orderProductDetail.getProductId(), orderProductDetail.getQuantity());
    }

    return orderStoreDetailRepository.updateStatus(status, orderStoreDetailId);
  }

  @Override
  public Integer getStoreProductCount() {
    try {
      Optional<String> username = SecurityUtils.getCurrentUserLogin();

      if (username.isPresent()) {
        Optional<Long> storeId = userRepository.getStoreId(username.get());

        if (storeId.isPresent()) {
          return productRepository.getStoreProductCount(storeId.get());
        }
      }
    } catch (Exception e) {
      log.error("Get getStoreProductCount was failure, error={}", e.getMessage());
      throw e;
    }
    return 0;
  }

  @Override
  public Integer getOrderStoreDetailCount() {
    try {
      Optional<String> username = SecurityUtils.getCurrentUserLogin();

      if (username.isPresent()) {
        Optional<Long> storeId = userRepository.getStoreId(username.get());

        if (storeId.isPresent()) {
          return orderStoreDetailRepository.getOrderStoreDetailCount(storeId.get());
        }
      }
    } catch (Exception e) {
      log.error("Get getOrderStoreDetailCount was failure, error={}", e.getMessage());
      throw e;
    }
    return 0;
  }
}
