package io.karhit.ecommerce.service.impl;

import io.karhit.ecommerce.entity.Enum.OrderStatus;
import io.karhit.ecommerce.entity.OrderEntity;
import io.karhit.ecommerce.entity.OrderProductDetailEntity;
import io.karhit.ecommerce.entity.OrderStoreDetailEntity;
import io.karhit.ecommerce.entity.UserEntity;
import io.karhit.ecommerce.entity.request.CheckoutRequest;
import io.karhit.ecommerce.exception.DatabaseException;
import io.karhit.ecommerce.exception.ResourcesNotFoundException;
import io.karhit.ecommerce.repository.OrderProductDetailRepository;
import io.karhit.ecommerce.repository.OrderRepository;
import io.karhit.ecommerce.repository.OrderStoreDetailRepository;
import io.karhit.ecommerce.repository.ProductRepository;
import io.karhit.ecommerce.repository.UserRepository;
import io.karhit.ecommerce.service.OrderService;
import io.karhit.ecommerce.util.SecurityUtils;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
public class OrderServiceImpl implements OrderService {

  private final OrderRepository orderRepository;
  private final OrderStoreDetailRepository orderStoreDetailRepository;
  private final OrderProductDetailRepository orderProductDetailRepository;
  private final UserRepository userRepository;
  private final ProductRepository productRepository;

  public OrderServiceImpl(OrderRepository orderRepository, OrderStoreDetailRepository orderStoreDetailRepository,
      OrderProductDetailRepository orderProductDetailRepository, UserRepository userRepository,
      ProductRepository productRepository) {
    this.orderRepository = orderRepository;
    this.orderStoreDetailRepository = orderStoreDetailRepository;
    this.orderProductDetailRepository = orderProductDetailRepository;
    this.userRepository = userRepository;
    this.productRepository = productRepository;
  }

  @Override
  public OrderEntity getOrderById(Long orderId) {
    Optional<OrderEntity> orderEntity = orderRepository.getOrderById(orderId);

    if (!orderEntity.isPresent()) {
      throw new ResourcesNotFoundException(
          String.format("Order with id = %s not found", orderId));
    }

    return orderEntity.get();
  }

  @Override
  public OrderStoreDetailEntity getOrderStoreDetailById(Long orderStoreDetailId) {
    Optional<OrderStoreDetailEntity> orderStoreDetailEntity =
        orderStoreDetailRepository.getOrderStoreDetailById(orderStoreDetailId);

    if (!orderStoreDetailEntity.isPresent()) {
      throw new ResourcesNotFoundException(
          String.format("Order Store Detail with id = %s not found", orderStoreDetailId));
    }

    return orderStoreDetailEntity.get();
  }

  @Override
  public OrderProductDetailEntity getOrderProductDetailById(Long orderProductDetailId) {
    Optional<OrderProductDetailEntity> orderProductDetailEntity =
        orderProductDetailRepository.getOrderProductDetailById(orderProductDetailId);

    if (!orderProductDetailEntity.isPresent()) {
      throw new ResourcesNotFoundException(
          String.format("Order Product Detail with id = %s not found", orderProductDetailId));
    }

    return orderProductDetailEntity.get();
  }

  @Override
  public List<OrderProductDetailEntity> getOrderProductDetailsByOrderStoreDetailId(Long orderStoreDetailId) {

    return orderProductDetailRepository.getAllByOrderStoreDetailId(orderStoreDetailId);
  }

  @Override
  public Collection<OrderStoreDetailEntity> getOrderStoreDetailsByOrderId(Long orderId) {

    return orderStoreDetailRepository.getALLByOrderId(orderId);
  }

  @Transactional
  @Override
  public Collection<OrderEntity> getOrdersFromUser(Integer limit, Integer offset) {
    try {
      Optional<String> username = SecurityUtils.getCurrentUserLogin();

      if (username.isPresent()) {
        Optional<UserEntity> user = userRepository.getUserByUserName(username.get());

        if (user.isPresent()) {
          long userId = user.get().getId();

          Collection<OrderEntity> orderEntities = orderRepository.getAllByUserId(userId, limit, offset);

          for (OrderEntity orderEntity : orderEntities) {

            Collection<OrderStoreDetailEntity> orderStoreDetailEntities = orderStoreDetailRepository.getALLByOrderId(orderEntity.getId());

            for (OrderStoreDetailEntity orderStoreDetail : orderStoreDetailEntities) {


              Collection<OrderProductDetailEntity> orderProductDetailEntities = orderProductDetailRepository.getAllByOrderStoreDetailId(
                  orderStoreDetail.getId());

              orderStoreDetail.setOrderProductDetails(orderProductDetailEntities);

            }

            orderEntity.setOrderStoreDetails(orderStoreDetailEntities);
          }
          return orderEntities;
        }
      }
    } catch (Exception e) {
      log.error(e.getMessage());
      throw e;
    }

    return Collections.emptyList();
  }

  @Transactional
  @Override
  public Collection<OrderEntity> getOrdersFromStore(Integer limit, Integer offset) {
    try {
      Optional<String> username = SecurityUtils.getCurrentUserLogin();

      if (username.isPresent()) {
        Optional<UserEntity> user = userRepository.getUserByUserName(username.get());

        if (user.isPresent()) {
          long storeId = user.get().getStoreId();

          Collection<OrderEntity> orderEntities = orderRepository.getAllByStoreId(storeId, limit, offset);

          for (OrderEntity orderEntity : orderEntities) {

            BigDecimal totalPrice = BigDecimal.valueOf(0);

            Collection<OrderStoreDetailEntity> orderStoreDetailEntities =
                orderStoreDetailRepository.getALLByOrderIdAndStoreId(orderEntity.getId(), storeId);

            for (OrderStoreDetailEntity orderStoreDetail : orderStoreDetailEntities) {

              Collection<OrderProductDetailEntity> orderProductDetailEntities = orderProductDetailRepository.getAllByOrderStoreDetailId(
                  orderStoreDetail.getId());

              orderStoreDetail.setOrderProductDetails(orderProductDetailEntities);
              totalPrice = totalPrice.add(orderStoreDetail.getTotalPrice());

            }

            orderEntity.setOrderStoreDetails(orderStoreDetailEntities);
            orderEntity.setTotalPrice(totalPrice);
          }
          return orderEntities;
        }
      }
    } catch (Exception e) {
      log.error(e.getMessage());
      throw e;
    }

    return Collections.emptyList();
  }

  @Transactional
  @Override
  public Long save(CheckoutRequest checkoutRequest) throws DatabaseException {
    Long orderId = System.currentTimeMillis();
    OrderEntity orderEntity = OrderEntity.builder()
        .id(orderId)
        .userId(checkoutRequest.getUserId())
        .totalPrice(checkoutRequest.getTotalPrice())
        .receiverName(checkoutRequest.getReceiverName())
        .phone(checkoutRequest.getPhone())
        .email(checkoutRequest.getEmail())
        .houseNumberStreet(checkoutRequest.getHouseNumberStreet())
        .address(checkoutRequest.getAddress())
        .status(OrderStatus.CREATED)
        .shipmentMethod(checkoutRequest.getShipmentMethod())
        .paymentMethod(checkoutRequest.getPaymentMethod())
        .createdAt(new Date())
        .build();
    orderRepository.save(orderEntity);

    for (CheckoutRequest.OrderStoreDetailRequest orderStoreDetailRequest : checkoutRequest.getOrderStoreDetails()) {
      Long orderStoreDetailId = System.currentTimeMillis();

      OrderStoreDetailEntity orderStoreDetailEntity =
          OrderStoreDetailEntity.builder()
              .id(orderStoreDetailId)
              .orderId(orderId)
              .storeId(orderStoreDetailRequest.getStoreId())
              .storeName(orderStoreDetailRequest.getStoreName())
              .totalPrice(orderStoreDetailRequest.getTotalPrice())
              .status(OrderStatus.CREATED)
              .build();

      orderStoreDetailRepository.save(orderStoreDetailEntity);

      for (CheckoutRequest.OrderStoreDetailRequest.OrderProductDetailRequest orderProductDetailRequest :
          orderStoreDetailRequest.getOrderProductDetails()) {
        Long orderProductDetailId = System.currentTimeMillis();

        Integer stock = productRepository.getStock(orderProductDetailRequest.getProductId());

        if (stock < orderProductDetailRequest.getQuantity()) {
          throw new DatabaseException("Not enough stock!");
        }

        OrderProductDetailEntity orderProductDetailEntity =
            OrderProductDetailEntity.builder()
                .id(orderProductDetailId)
                .orderStoreDetailId(orderStoreDetailId)
                .productId(orderProductDetailRequest.getProductId())
                .productName(orderProductDetailRequest.getProductName())
                .quantity(orderProductDetailRequest.getQuantity())
                .price(orderProductDetailRequest.getPrice())
                .totalPrice(orderProductDetailRequest.getTotalPrice())
                .build();

        orderProductDetailRepository.save(orderProductDetailEntity);

        productRepository.subtractStock(orderProductDetailRequest.getProductId(), orderProductDetailRequest.getQuantity());
      }
    }

    return orderId;
  }

  @Override
  public Integer getUserOrderCount() throws DatabaseException {
    try {
      Optional<String> username = SecurityUtils.getCurrentUserLogin();

      if (username.isPresent()) {
        Optional<UserEntity> user = userRepository.getUserByUserName(username.get());

        if (user.isPresent()) {
          long userId = user.get().getId();

          return orderRepository.getUserOrderCount(userId);
        }
      }
    } catch (Exception e) {
      log.error(e.getMessage());
      throw e;
    }

    return 0;
  }

  @Override
  public Integer getStoreOrderCount() throws DatabaseException {
    try {
      Optional<String> username = SecurityUtils.getCurrentUserLogin();

      if (username.isPresent()) {
        Optional<UserEntity> user = userRepository.getUserByUserName(username.get());

        if (user.isPresent()) {
          long storeId = user.get().getStoreId();

          return orderRepository.getStoreOrderCount(storeId);
        }
      }
    } catch (Exception e) {
      log.error(e.getMessage());
      throw e;
    }

    return 0;
  }
}
