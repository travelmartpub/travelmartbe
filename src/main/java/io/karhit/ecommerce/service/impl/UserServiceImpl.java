package io.karhit.ecommerce.service.impl;

import io.karhit.ecommerce.entity.UserAddressEntity;
import io.karhit.ecommerce.entity.UserEntity;
import io.karhit.ecommerce.entity.request.UserAddressRequest;
import io.karhit.ecommerce.exception.DatabaseException;
import io.karhit.ecommerce.repository.UserAddressRepository;
import io.karhit.ecommerce.repository.UserAuthorityRepository;
import io.karhit.ecommerce.repository.UserRepository;
import io.karhit.ecommerce.service.UserService;
import io.karhit.ecommerce.util.SecurityUtils;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserAuthorityRepository userAuthorityRepository;
    private final UserAddressRepository userAddressRepository;

    public UserServiceImpl(UserRepository userRepository,
        UserAuthorityRepository userAuthorityRepository,
        UserAddressRepository userAddressRepository) {
        this.userRepository = userRepository;
        this.userAuthorityRepository = userAuthorityRepository;
        this.userAddressRepository = userAddressRepository;
    }

    @Transactional
    @Override
    public Optional<UserEntity> getUserWithAuthorities() {

        Optional<String> username = SecurityUtils.getCurrentUserLogin();

        if (username.isPresent()) {
            Optional<UserEntity> userEntity = userRepository.getUserByUserName(username.get());

            userEntity.ifPresent(
                user -> user.setAuthorities(userAuthorityRepository.getAuthoritiesByUserId(user.getId())));
            return userEntity;
        }

        return Optional.empty();
    }

    @Override
    public Optional<UserAddressEntity> getUserAddress() {
        Optional<String> username = SecurityUtils.getCurrentUserLogin();

        if (username.isPresent()) {
            return userAddressRepository.getUserAddresses(username.get());
        }

        return Optional.empty();
    }

    @Transactional
    @Override
    public UserAddressEntity saveUserAddress(UserAddressRequest userAddressRequest) throws DatabaseException {
        Optional<String> username = SecurityUtils.getCurrentUserLogin();

        if (username.isPresent()) {
            Optional<UserAddressEntity> userAddress = userAddressRepository.getUserAddresses(username.get());
            if (userAddress.isPresent()) {

                userAddressRepository.update(userAddressRequest, username.get());

                UserAddressEntity userAddressEntity = new UserAddressEntity();
                userAddressEntity.setId(null);
                userAddressEntity.setUsername(username.get());
                userAddressEntity.setPhone(userAddressRequest.getPhone());
                userAddressEntity.setHouseNumberStreet(userAddressRequest.getHouseNumberStreet());
                userAddressEntity.setAddress(userAddressRequest.getAddress());

                return userAddressEntity;
            } else {
                Long id = System.currentTimeMillis();
                UserAddressEntity userAddressEntity = new UserAddressEntity();
                userAddressEntity.setId(id);
                userAddressEntity.setUsername(username.get());
                userAddressEntity.setPhone(userAddressRequest.getPhone());
                userAddressEntity.setHouseNumberStreet(userAddressRequest.getHouseNumberStreet());
                userAddressEntity.setAddress(userAddressRequest.getAddress());
                return userAddressRepository.save(userAddressEntity);
            }
        } else {
            return null;
        }
    }
}
