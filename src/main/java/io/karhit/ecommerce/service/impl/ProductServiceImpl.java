package io.karhit.ecommerce.service.impl;

import io.karhit.ecommerce.entity.ProductEntity;
import io.karhit.ecommerce.entity.request.ProductRequest;
import io.karhit.ecommerce.exception.DatabaseException;
import io.karhit.ecommerce.exception.ResourcesNotFoundException;
import io.karhit.ecommerce.repository.ProductRepository;
import io.karhit.ecommerce.service.ProductService;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ProductServiceImpl implements ProductService {

  private final ProductRepository productRepository;

  public ProductServiceImpl(ProductRepository productRepository) {
    this.productRepository = productRepository;
  }

  @Override
  public Integer getCount() throws DatabaseException {
    return productRepository.getCount();
  }

  @Override
  public Integer getStoreProductCount(Long storeId) throws DatabaseException {
    return productRepository.getStoreProductCount(storeId);
  }

  @Override
  public Collection<ProductEntity> getPagingProducts(Integer limit, Integer offset, String orderBy) {
    Collection<ProductEntity> products;
    try {
      products = productRepository.getPagingProducts(limit, offset, orderBy);

      log.info("Get PagingProducts with limit={}, offset = {}, orderBy = {}", limit, offset, orderBy);
    } catch (Exception e) {
      log.error("Get PagingProducts fail with limit={}, offset = {}, orderBy = {}, error = {}", limit, offset,
          orderBy, e.getMessage());
      throw e;
    }
    return products;
  }

  @Override
  public Collection<ProductEntity> getStoreProducts(Long storeId, Integer limit, Integer offset) {
    Collection<ProductEntity> products;
    try {
      products = productRepository.getStorePagingProducts(storeId, limit, offset, null);

    } catch (Exception e) {
      log.error("Get getStoreProducts fail with storeId={} limit={}, offset = {}, error = {}", storeId, limit, offset, e.getMessage());
      throw e;
    }
    return products;
  }

  @Override
  public List<ProductEntity> getAll(Set<Long> pIds) {
    List<ProductEntity> products;
    pIds = pIds.size() > 20 ? pIds.stream().limit(20).collect(Collectors.toSet()) : pIds;
    try {
      products = productRepository.getAllByActive(pIds, true);

      log.info("Get products with pIds={}", pIds);
    } catch (Exception e) {
      log.error("Get products was failure pIds={}, error={}", pIds, e.getMessage());
      throw e;
    }

    return products;
  }

  @Override
  public ProductEntity getById(Long pId) {
    Optional<ProductEntity> product = productRepository.getProductByIdAndActive(pId, true);

    if (!product.isPresent()) {
      throw new ResourcesNotFoundException(
        String.format("Product with id = %s not found", pId));
    }

    return product.get();
  }

  @Override
  public Long getStoreId(Long productId) {
    Optional<Long> storeId = productRepository.getStoreId(productId);

    if (!storeId.isPresent()) {
      throw new ResourcesNotFoundException(
        String.format("Product with id = %s not found", productId));
    }

    return storeId.get();
  }

  @Override
  public ProductEntity save(ProductRequest productRequest) throws DatabaseException {
    long productId = System.currentTimeMillis();

    ProductEntity productEntity = ProductEntity.builder()
        .id(productId)
        .name(productRequest.getName())
        .storeId(productRequest.getStoreId())
        .description(productRequest.getDescription())
        .price(productRequest.getPrice())
        .stock(productRequest.getStock())
        .sold(0)
        .createdAt(new Date())
        .images(productRequest.getImages())
      .activated(true)
      .build();

    productEntity = productRepository.save(productEntity);

    return productEntity;
  }
}
