package io.karhit.ecommerce.service;

import io.karhit.ecommerce.entity.Enum.OrderStatus;
import io.karhit.ecommerce.entity.OrderStoreDetailEntity;
import io.karhit.ecommerce.entity.ProductEntity;
import io.karhit.ecommerce.entity.StoreEntity;
import io.karhit.ecommerce.entity.request.CreateStoreRequest;
import io.karhit.ecommerce.entity.request.ProductRequest;
import io.karhit.ecommerce.exception.DatabaseException;
import java.util.Collection;
import org.springframework.transaction.annotation.Transactional;

public interface StoreService {

  StoreEntity getById(Long storeId);

  StoreEntity getActivatedById(Long storeId);

  StoreEntity save(CreateStoreRequest createStoreRequest) throws DatabaseException;

  @Transactional
  StoreEntity createUserStore(CreateStoreRequest createStoreRequest) throws DatabaseException;

  @Transactional
  Collection<ProductEntity> getPagingProducts(Integer limit, Integer offset, String orderBy);

  ProductEntity createProduct(ProductRequest productRequest) throws DatabaseException;

  ProductEntity updateProduct(ProductRequest productRequest) throws DatabaseException;

  Long deactivateProduct(Long productId, boolean activated) throws DatabaseException;

  Collection<OrderStoreDetailEntity> getOrderStoreDetail(Integer limit, Integer offset);

  Long updateOrderStoreDetailStatus(OrderStatus status, Long orderStoreDetailId) throws DatabaseException;

  Integer getStoreProductCount();

  Integer getOrderStoreDetailCount();
}
