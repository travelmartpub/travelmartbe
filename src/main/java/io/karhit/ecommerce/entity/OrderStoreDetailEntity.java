package io.karhit.ecommerce.entity;

import io.karhit.ecommerce.entity.Enum.OrderStatus;
import java.math.BigDecimal;
import java.util.Collection;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderStoreDetailEntity {

  private Long id;
  private Long orderId;
  private Long storeId;
  private String storeName;
  private BigDecimal totalPrice;
  private OrderStatus status;

  private Collection<OrderProductDetailEntity> orderProductDetails;
}
