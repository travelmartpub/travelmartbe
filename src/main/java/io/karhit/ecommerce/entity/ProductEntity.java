package io.karhit.ecommerce.entity;

import java.math.BigDecimal;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductEntity {
    private Long id;
    private String name;
    private Long storeId;
    private String description;
    private BigDecimal price;
    private Integer stock;
    private Integer sold;
    private Date createdAt;
    private String images;
    private boolean activated = true;
}
