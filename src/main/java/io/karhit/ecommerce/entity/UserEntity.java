package io.karhit.ecommerce.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Collection;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserEntity {

  private Long id;
  private String userName;
  @JsonIgnore
  private String password;
  private String email;
  private Long storeId;
  private String avatar;
  private boolean activated = true;
  private Collection<UserAuthorityEntity> authorities;

  public UserEntity(UserEntity userEntity) {
    this.id = userEntity.id;
    this.userName = userEntity.userName;
    this.password = userEntity.password;
    this.email = userEntity.email;
    this.storeId = userEntity.storeId;
    this.avatar = userEntity.avatar;
    this.activated = userEntity.activated;
  }
}
