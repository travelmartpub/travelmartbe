package io.karhit.ecommerce.entity;

import io.karhit.ecommerce.entity.Enum.OrderStatus;
import io.karhit.ecommerce.entity.Enum.PaymentMethod;
import io.karhit.ecommerce.entity.Enum.ShipmentMethod;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderEntity {

  private Long id;
  private Long userId;
  private BigDecimal totalPrice;
  private String receiverName;
  private String email;
  private String phone;
  private String houseNumberStreet;
  private String address;
  private OrderStatus status;
  private ShipmentMethod shipmentMethod;
  private PaymentMethod paymentMethod;
  private Date createdAt;

  private Collection<OrderStoreDetailEntity> orderStoreDetails;
}
