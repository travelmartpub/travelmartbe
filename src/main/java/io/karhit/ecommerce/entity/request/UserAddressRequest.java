package io.karhit.ecommerce.entity.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserAddressRequest {

  private String houseNumberStreet;
  private String address;
  private String phone;
}
