package io.karhit.ecommerce.entity.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * DTO for storing a user's credentials.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LoginReq {

    private String username;
    private String password;
    private Boolean rememberMe;

}
