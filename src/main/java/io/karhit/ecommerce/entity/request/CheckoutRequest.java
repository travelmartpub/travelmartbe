package io.karhit.ecommerce.entity.request;

import io.karhit.ecommerce.entity.Enum.OrderStatus;
import io.karhit.ecommerce.entity.Enum.PaymentMethod;
import io.karhit.ecommerce.entity.Enum.ShipmentMethod;
import java.math.BigDecimal;
import java.util.Collection;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CheckoutRequest {

  private Long userId;
  private Collection<OrderStoreDetailRequest> orderStoreDetails;
  private BigDecimal totalPrice;
  private String receiverName;
  private String email;
  private String phone;
  private String houseNumberStreet;
  private String address;
  private ShipmentMethod shipmentMethod;
  private PaymentMethod paymentMethod;
  private OrderStatus status;

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  @Builder
  @Getter
  public static class OrderStoreDetailRequest {

    private Long storeId;
    private String storeName;
    private Collection<OrderProductDetailRequest> orderProductDetails;
    private BigDecimal totalPrice;
    private OrderStatus status;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    @Getter
    public static class OrderProductDetailRequest {

      private Long productId;
      private String productName;
      private Integer quantity;
      private BigDecimal price;
      private BigDecimal totalPrice;
    }
  }
}