package io.karhit.ecommerce.entity.response;

import com.google.common.base.Strings;
import java.util.Collections;
import java.util.List;

public class ErrorResponse {

  private ErrorObject error;

  public ErrorResponse(ErrorConstants errorConstants, List<Object> details) {
    if (details == null) {
      details = Collections.emptyList();
    }

    this.error = new ErrorObject(errorConstants, details);
  }

  public ErrorResponse() {
  }

  public ErrorObject getError() {
    return error;
  }

  public void setError(ErrorObject error) {
    this.error = error;
  }

  public static class ErrorObject {

    public static final ErrorObject SUCCESSFUL = new ErrorObject(ErrorConstants.SUCCESSFUL,
        Collections.emptyList());

    private int code;
    private String message;
    private List<Object> details;

    public ErrorObject(ErrorConstants errorConstants, List<Object> details) {
      this.code = errorConstants.getCode();
      this.message = errorConstants.getMessage();
      this.details = details;
    }

    public ErrorObject(ErrorConstants errorConstants) {
      this.code = errorConstants.getCode();
      this.message = errorConstants.getMessage();
      this.details = Collections.emptyList();
    }

    public String getMessage() {
      return message;
    }

    public void setMessage(String message) {
      this.message = message;
    }

    public List<Object> getDetails() {
      return details;
    }

    public void setDetails(List<Object> details) {
      this.details = details;
    }

    public int getCode() {
      return code;
    }

    public void setCode(int code) {
      this.code = code;
    }
  }


}
