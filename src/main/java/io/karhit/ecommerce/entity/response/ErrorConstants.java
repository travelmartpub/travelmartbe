package io.karhit.ecommerce.entity.response;

public enum ErrorConstants {

  SUCCESSFUL(1, "Transaction successful"),
  FAILURE(-1, "FAILURE"),
  REGISTER_FAIL(-2, "Register fail"),
  AUTH_FAIL(-3, "Authentication fail");

  private int code;
  private String message;

  ErrorConstants(int code, String message) {
    this.code = code;
    this.message = message;
  }

  public int getCode() {
    return code;
  }

  public String getMessage() {
    return message;
  }
}
