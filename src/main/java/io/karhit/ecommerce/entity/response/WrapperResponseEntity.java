package io.karhit.ecommerce.entity.response;

import io.karhit.ecommerce.entity.response.ErrorResponse.ErrorObject;
import java.util.Collections;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class WrapperResponseEntity {

    private ErrorObject error;
    private Object data;
    private PagingResponse paging;

    public WrapperResponseEntity(ErrorObject error, Object data) {
        this.error = error;
        this.data = data;
    }

    public WrapperResponseEntity(ErrorObject error, Object data, PagingResponse paging) {
        this.error = error;
        this.data = data;
        this.paging = paging;
    }

    public WrapperResponseEntity(Object data) {
        this.error = ErrorObject.SUCCESSFUL;
        this.data = data;
    }

    public WrapperResponseEntity(Object data, PagingResponse paging) {
        this.error = ErrorObject.SUCCESSFUL;
        this.data = data;
        this.paging = paging;
    }

    public WrapperResponseEntity(ErrorObject error) {
        this.error = error;
        this.data = Collections.emptyList();
    }

    public WrapperResponseEntity() {
        this.error = ErrorObject.SUCCESSFUL;
        this.data = Collections.emptyList();
    }
}
