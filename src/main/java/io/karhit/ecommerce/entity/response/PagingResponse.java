package io.karhit.ecommerce.entity.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PagingResponse {

    private Integer limit;
  private Integer offset;
  private String orderBy;
    private Integer total;
}
