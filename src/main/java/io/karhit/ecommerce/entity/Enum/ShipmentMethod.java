package io.karhit.ecommerce.entity.Enum;

import lombok.Getter;

@Getter
public enum ShipmentMethod {
  RECEIVE_AT_STORE("receive_at_store"),
  SHIPPING("shipping");

  private final String value;

  ShipmentMethod(String status) {
    this.value = status;
  }
}
