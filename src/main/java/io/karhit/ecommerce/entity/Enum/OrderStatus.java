package io.karhit.ecommerce.entity.Enum;

import lombok.Getter;

@Getter
public enum OrderStatus {
  CREATED("created"),
  PAID("paid"),
  ACCEPTED("accepted"),
  PACKAGED("packaged"),
  SHIPPED("shipped"),
  RECEIVED("received"),
  COMPLETED("completed"),
  CANCELLED("cancelled");

  private final String value;

  OrderStatus(String status) {
    this.value = status;
  }
}
