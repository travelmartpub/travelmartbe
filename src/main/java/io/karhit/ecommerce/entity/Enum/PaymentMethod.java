package io.karhit.ecommerce.entity.Enum;

import lombok.Getter;

@Getter
public enum PaymentMethod {
  COD("cod"),
  ZALOPAY("zalopay"),
  MOMO("momo");

  private final String value;

  PaymentMethod(String status) {
    this.value = status;
  }
}
