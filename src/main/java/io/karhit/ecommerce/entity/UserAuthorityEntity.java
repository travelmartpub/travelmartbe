package io.karhit.ecommerce.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * A mapping user authority (a security role) used by Spring Security.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserAuthorityEntity {

  private Long userId;
  private String name;

}
