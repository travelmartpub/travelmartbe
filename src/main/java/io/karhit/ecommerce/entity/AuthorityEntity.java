package io.karhit.ecommerce.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * An authority (a security role) used by Spring Security.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AuthorityEntity {

  private String name;

}
