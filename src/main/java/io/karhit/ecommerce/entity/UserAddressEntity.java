package io.karhit.ecommerce.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserAddressEntity {

  private Long id;
  private String username;
  private String houseNumberStreet;
  private String address;
  private String phone;
}
