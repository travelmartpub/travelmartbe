default: docker_build

DOCKER_IMAGE ?= us-east1-docker.pkg.dev/project-heterr/docker-repo/ecommerce
HELM_CHART ?= us-central1-docker.pkg.dev/project-heterr/helm-repo/ecommerce
TAG = 1.0.0-bd841023

docker_build:
	DOCKER_BUILDKIT=1 docker build \
		--build-arg ENV=dev \
		-t $(DOCKER_IMAGE):$(TAG) .

docker_run:
	docker run -it --rm \
		--publish 8083:8083 \
		--name ecommerce \
		$(DOCKER_IMAGE):$(TAG)

docker_push:
	docker push $(DOCKER_IMAGE):$(TAG)

helm_install:
	helm upgrade --install ecommerce \
		oci://$(HELM_CHART) --version $(TAG) \
		--namespace karhit --create-namespace --debug