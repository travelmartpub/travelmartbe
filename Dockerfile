# syntax=docker/dockerfile:experimental
FROM maven:3-jdk-8 as builder

WORKDIR /build
COPY . .

ARG MAVEN_OPTS

RUN --mount=type=cache,id=cache-maven,target=/root/.m2 mvn clean package -DskipTests

# Alpine Linux with OpenJDK JRE
FROM mtsinichi/karhit-openjdk:8-jdk-slim

ENV HOME /home/deploy_karhit
WORKDIR $HOME

ARG ENV
ENV APP_ENV=${ENV}

COPY --from=builder --chown=deploy_karhit:deploy_karhit /build/dist/*.jar $HOME
COPY --chown=deploy_karhit:deploy_karhit run.sh $HOME

# For application
COPY --chown=deploy_karhit:deploy_karhit logback-spring.xml $HOME
COPY --chown=deploy_karhit:deploy_karhit conf/$APP_ENV.yml /app/config/release.yml

RUN chmod +x $HOME/run.sh

# Service
EXPOSE 8083

ENTRYPOINT ["/home/deploy_karhit/run.sh"]
